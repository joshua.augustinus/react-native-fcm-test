# Warning
This app has only been tested on Android devices.

# Developing
Open a command window on FcmTest and run:
```
yarn start
```

Then open another command window on the same folder and run:
```
yarn debug
```

This script will do a TypeScript check as well as install the app (see package.json for what it does)

This version of the app only works while the tablet is connected via USB. To have it work while not connected use:
```
yarn build
```

# Debugging java

Open the project in Android Studio (just the Android folder) then click the bug icon at the top. You can also go Run>Debug. This technique allows you to hit breakpoints in Android Studio.

The tablet will need to be plugged in even after the apk is deployed.

I don't think this technique works when trying to debug when launching the app via FirebaseCloudMessage


# Testing using detox
Open a command window 

On FcmTest run:
```
yarn start
```

Next on FcmTest run:
```
yarn detox-prepare
```
This should build a file. Make sure it exists:
```
FcmTest\android\app\build\outputs\apk\androidTest
```
You can skip this step in the future once it has already been built.

Next build our app. With the following script we will set the app to use .env.test 
```
yarn e2e
```
You should see the e2e home page now on the app.
Finally run:
```
yarn detox-debug
```