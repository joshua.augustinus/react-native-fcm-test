import 'react-native-gesture-handler'; //Need for react navigation https://reactnavigation.org/docs/getting-started
import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import React from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import store from './src/store';
import {navigationRef} from '@src/RootNavigation';
import {theme} from '@src/theme';

const Root = (props) => (
  <Provider store={store}>
    <PaperProvider theme={theme}>
      <NavigationContainer ref={navigationRef}>
        <App {...props} />
      </NavigationContainer>
    </PaperProvider>
  </Provider>
);

AppRegistry.registerComponent(appName, () => Root);
