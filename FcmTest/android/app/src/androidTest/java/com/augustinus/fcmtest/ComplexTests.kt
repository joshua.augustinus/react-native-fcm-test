package com.augustinus.fcmtest

import android.Manifest
import androidx.test.filters.LargeTest
import androidx.test.rule.GrantPermissionRule
import com.augustinus.fcmtest.util.*
import com.augustinus.fcmtest.util.UiAutomatorHelper.tapText
import com.augustinus.fcmtest.util.UiAutomatorHelper.waitForText
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.lang.Integer.parseInt


@RunWith(JUnit4::class)
@LargeTest
class ComplexTests {
    @Rule
    @JvmField
    var permissionLocation = GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION)

    @Rule
    @JvmField
    var permissionCamera = GrantPermissionRule.grant(Manifest.permission.CAMERA)

    @Rule
    @JvmField
    var permissionAudio = GrantPermissionRule.grant(Manifest.permission.RECORD_AUDIO)

    @Rule
    @JvmField
    var permissionRead = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE)

    @Rule
    @JvmField
    var permissionWrite = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE)

    @Before
    fun beforeEach() {
        SetupHelper.setup()
    }

    @After
    fun cleanup() {
        UiAutomatorHelper.closeApps();
    }

    companion object {
        @BeforeClass
        @JvmStatic
        fun setup() {
            SetupHelper.setup()
            tapText("Environment: TEST")
            waitForText("E2E Testing Home")
            tapText("RESET STATE")
            tapText("GO TO TRUE HOME PAGE")
            waitForText("Login")

            TestHelper.inputText("USERNAME_INPUT", "demo!")
            TestHelper.inputText("PASSWORD_INPUT", "demo!")
            tapText("NEXT")
            waitForText("JOIN WAITING ROOM")
            TestHelper.scrollToComponent("PLAY_SOUNDS_CHECKBOX")
            TestHelper.tap("PLAY_SOUNDS_CHECKBOX")
        }
    }

    @Test
    fun receiveCallWhenAppClosed() {
        TestHelper.tap("JOIN_WAITING_BUTTON")
        TestHelper.screenshot("SelectDoctor")
        TestHelper.tap("NEXT_AVAILABLE_BUTTON")
        UiAutomatorHelper.waitForText("Waiting Room");

        TestHelper.sleep(2000);

        val fcmToken = PreferenceHelper.loadItem(SetupHelper.mainActivity, R.string.preference_fcm_token);
        val ticketNumber = PreferenceHelper.loadItem(SetupHelper.mainActivity, R.string.preference_ticket_number);
        TestHelper.log("fcmToken: $fcmToken");
        TestHelper.log("ticketNumber: $ticketNumber");


        UiAutomatorHelper.closeAppsAndLock();
        TestHelper.sleep(2000);
        ApiHelper.createNewSession(parseInt(ticketNumber), fcmToken);
        UiAutomatorHelper.waitForText("Decline call");
        UiAutomatorHelper.wakeDevice();
        TestHelper.tap("DECLINE_CALL_BUTTON")
        UiAutomatorHelper.waitForText("Waiting Room");
        TestHelper.tap("CANCEL_APPOINTMENT_BUTTON")
        UiAutomatorHelper.waitForText("Welcome")
    }

}