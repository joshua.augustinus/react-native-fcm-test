package com.augustinus.fcmtest.util;

import android.app.Activity;
import android.graphics.Bitmap;

import com.google.android.libraries.cloudtesting.screenshots.ScreenShotter;
import com.squareup.spoon.Spoon;

import java.io.IOException;

import androidx.test.runner.screenshot.ScreenCapture;
import androidx.test.runner.screenshot.Screenshot;

public class ScreenShotHelper {
    public static void firebaseCapture(Activity activity, String name) {
        try {
            ScreenShotter.takeScreenshot(name, activity);
            TestHelper.log("Took firebase screenshot: " + name);
        } catch (Exception e) {
            TestHelper.log(e.getMessage());
        }
    }

    public static void spoonCapture(Activity activity, String name) {
        try {
            Spoon.screenshot(activity, name);
            TestHelper.log("Took spoon screenshot: " + name);
        } catch (Exception e) {
            TestHelper.log(e.getMessage());
        }

    }

    /**
     * This screenshot method will save to Pictures folder
     */
    public static void picturesCapture(Activity activity, String name) {
        CustomScreenCaptureProcessor processor = new CustomScreenCaptureProcessor();

        ScreenCapture capture = Screenshot.capture();
        capture.setFormat(Bitmap.CompressFormat.PNG);

        capture.setName(name);

        try {

            processor.process(capture);

            TestHelper.log("Took pictures capture: " + name);

        } catch (IOException ex) {
            TestHelper.log(ex.getMessage());
        }
    }
}
