package com.augustinus.fcmtest.util;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.hamcrest.Matcher;

import java.util.concurrent.TimeoutException;

import androidx.test.espresso.PerformException;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.util.HumanReadables;
import androidx.test.espresso.util.TreeIterables;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withTagValue;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.fail;

public class TestHelper {
    private static int screenshotCount = 0;
    private static boolean isFirstLoad = true;
    private static int EXPECT_MAX_TIME = 10000;

    /**
     * Will try for 5 seconds to find the element before tapping
     *
     * @param tagValue
     */
    public static void tap(String tagValue) {
        sleep(1000); //Needed to work on my Android phone
        expect(tagValue);

        ViewInteraction viewWithTagVI = onView(withTagValue(is((Object) tagValue)));
        viewWithTagVI.perform(click());
        log("tapped: " +tagValue);
    }

    public static void tapText(String textValue) {
        sleep(1000); //Needed to work on my Android phone


        ViewInteraction viewWithTagVI = onView(withText(textValue));
        viewWithTagVI.perform(click());
        log("tapped: " +textValue);
    }

    public static void tap(String tagValue, String parentTagValue) {
        expect(tagValue);
        ViewInteraction viewWithTagVI = onView(allOf(withTagValue(is((Object) tagValue)), isDescendantOfA(withTagValue(is((Object) parentTagValue)))));
        viewWithTagVI.perform(click());
    }

    public static void expect(String tagValue) {
        expect(tagValue, EXPECT_MAX_TIME);
    }

    public static void sleep(int milli) {
        try {
            Thread.sleep(milli);
            // Do some stuff
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public static void inputText(String tagValue, String text) {
        log("inputText tag: " + tagValue);
        tap(tagValue);
        log("tapped");
        sleep(1000);
        ViewInteraction viewWithTagVI = onView(withTagValue(is((Object) tagValue)));
        viewWithTagVI.perform(replaceText(text),pressImeActionButton());

        onView(allOf(withTagValue(is((Object) tagValue)), withText(text))).check(matches(isDisplayed()));
    }

    /**
     * Used in the scenario where we have multiple that match tagValue
     */
    public static void inputText(String tagValue, String parentTagValue, String text) {
        ViewInteraction viewWithTagVI = onView(allOf(withTagValue(is((Object) tagValue)), isDescendantOfA(withTagValue(is((Object) parentTagValue)))));
        viewWithTagVI.perform(click(), replaceText(text));
    }

    /**
     * You cannot use this while there is a splash screen
     */
    public static void expect(String tagValue, int waitMilli) {

        try {
            onView(isRoot()).perform(waitTag(tagValue, waitMilli));
            log("Found: " + tagValue);
        } catch (Exception e) {
            int sleep = 1000;
            int newWaitMilli = waitMilli - sleep;
            if (newWaitMilli <= 0) {
                fail("Couldn't find element with tag: " + tagValue);
            } else {
                log("Failed to find: " + tagValue);
                sleep(sleep);
                expect(tagValue, newWaitMilli);
            }

        }


    }

    public static String getText(String tagValue) {
        expect(tagValue);
        Matcher<View> matcher = withTagValue(is((Object) tagValue));
        return getText(matcher);
    }

    public static String getText(final Matcher<View> matcher) {
        try {
            final String[] stringHolder = {null};
            onView(matcher).perform(new ViewAction() {
                @Override
                public Matcher<View> getConstraints() {
                    return isAssignableFrom(TextView.class);
                }

                @Override
                public String getDescription() {
                    return "get text";
                }

                @Override
                public void perform(UiController uiController, View view) {
                    TextView tv = (TextView) view;
                    stringHolder[0] = tv.getText().toString();
                }
            });
            if (stringHolder[0] == null || stringHolder[0] == "") {
                fail("no text found");
            }
            return stringHolder[0];
        } catch (Exception e) {
            log(e.getMessage());
            fail("null found");
            return null;
        }

    }


    /**
     * Perform action of waiting for a specific view with tag.
     * https://stackoverflow.com/questions/49796132/android-espresso-wait-for-text-to-appear
     *
     * @param tag    The tag of the view to wait for.
     * @param millis The timeout of until when to wait for.
     */
    public static ViewAction waitTag(final String tag, final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for a specific view with tag <" + tag + "> during " + millis + " millis.";
            }

            @Override
            public void perform(final UiController uiController, final View view) {

                final Matcher<View> viewMatcher = withTagValue(is((Object) tag));


                for (View child : TreeIterables.breadthFirstViewTraversal(view)) {
                    // found view with required ID
                    if (viewMatcher.matches(child)) {
                        return;
                    }
                }


                // timeout happens
                throw new PerformException.Builder()
                        .withActionDescription(this.getDescription())
                        .withViewDescription(HumanReadables.describe(view))
                        .withCause(new TimeoutException())
                        .build();
            }
        };
    }

    public static void screenshot(String name) {
        sleep(2000); //Needed for animations to finish
        SetupHelper.screenshot(name);
        sleep(1000);
    }

    public static void log(String message) {
        Log.d("Espresso", message);
    }



    public static void scrollToComponent(String tagValue){
        ViewInteraction vi = onView(withTagValue(is((Object) tagValue)));
        vi.perform(scrollTo());
    }

    public static void swipeUp(){
        String tagValue = "SCROLLVIEW_TESTHOME";
        onView(withTagValue(is((Object) tagValue))).perform(ViewActions.swipeUp(), ViewActions.closeSoftKeyboard());
    }

    public static void assertFail(String message){
        fail(message);
    }
}
