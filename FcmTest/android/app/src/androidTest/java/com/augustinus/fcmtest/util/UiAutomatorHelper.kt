package com.augustinus.fcmtest.util

import android.bluetooth.BluetoothClass
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.*
import org.junit.Assert

object UiAutomatorHelper{
    val VIVO_LOCK_TITLE = "Tap on the lock to lock an app screen.";

    fun closeApps(){
        try {
            SetupHelper.mainActivity.finish()
            val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());


            if(isVivo())
            {
                TestHelper.log("Trying to close apps on VIVO device")
                uiDevice.pressHome()

            }else{
                //Close apps
                TestHelper.log("Trying to close apps")
                uiDevice.pressRecentApps();
                waitForText("Close all")

                TestHelper.sleep(500);
                val clearAll = uiDevice.findObject(By.text("Close all"));
                if(clearAll!=null)
                    clearAll.click();
            }


            TestHelper.log("Finished closing apps")

        }catch (e: Exception){
            TestHelper.log(e.message)
        }

    }

    fun isVivo():Boolean{
        val deviceType = getDeviceType()
        return deviceType.equals(DeviceTypes.VIVO_PHONE);
    }

    fun closeAppsAndLock(){
        closeApps();
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        //lock the screen
        uiDevice.sleep();
    }

    fun doesTextExist(text:String) : Boolean{
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        return uiDevice.findObject(By.textContains(text))!=null;
    }

    fun wakeDevice(){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        uiDevice.wakeUp();
    }

    fun waitForText(text:String){
        val waitTime:Long = 10000;
        waitForText(text,waitTime)
    }

    /**
     * Waits for text then taps it
     */
    fun tapText(text:String){
        waitForText(text)
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        uiDevice.findObject(By.textContains(text)).click()
    }

    fun waitForText(text:String, waitTime:Long){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        val result = uiDevice.wait(Until.hasObject(By.textContains(text)), waitTime);
        if(result==null || !result)
            Assert.fail("Couldn't find text: $text");

    }

    fun tap(x:Int, y:Int){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        uiDevice.click(x,y)
    }

    fun getDeviceDimensions() : Dimensions{
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        val height = uiDevice.displayHeight
        val width = uiDevice.displayWidth
        TestHelper.log("Dimensions width:$width, height:$height")

        return Dimensions(width, height)
    }

    fun getDeviceType():String{
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        val productName = uiDevice.getProductName();
        val displayHeight = uiDevice.displayHeight;
        val displayWidth = uiDevice.displayWidth;

        TestHelper.log("Product name: $productName")
        TestHelper.log("Display Width: $displayWidth Display Height: $displayHeight")
        return productName;
    }
}