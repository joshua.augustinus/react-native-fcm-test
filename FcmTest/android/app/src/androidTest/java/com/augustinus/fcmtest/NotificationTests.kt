package com.augustinus.fcmtest



import android.Manifest
import androidx.test.espresso.Espresso.pressBackUnconditionally
import androidx.test.filters.LargeTest
import androidx.test.rule.GrantPermissionRule
import com.augustinus.fcmtest.util.*
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.lang.Integer.parseInt


@RunWith(JUnit4::class)
@LargeTest
class NotificationTests {
    @Rule
    @JvmField
    var permissionLocation = GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION)

    @Rule
    @JvmField
    var permissionCamera = GrantPermissionRule.grant(Manifest.permission.CAMERA)

    @Rule
    @JvmField
    var permissionAudio = GrantPermissionRule.grant(Manifest.permission.RECORD_AUDIO)

    @Rule
    @JvmField
    var permissionRead = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE)

    @Rule
    @JvmField
    var permissionWrite = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE)

    @Before
    fun beforeEach() {

        SetupHelper.setup()
    }

    @After
    fun cleanup() {
        TestHelper.log("cleanup: Finished test")
    }

    companion object {
        @BeforeClass
        @JvmStatic
        fun setup() {
            SetupHelper.setup()
            UiAutomatorHelper.tapText("Environment: TEST")
            UiAutomatorHelper.waitForText("E2E Testing Home")
            UiAutomatorHelper.tapText("RESET STATE")
            UiAutomatorHelper.tapText("GO TO TRUE HOME PAGE")
            UiAutomatorHelper.waitForText("Login")

            TestHelper.inputText("USERNAME_INPUT", "demo!")
            TestHelper.inputText("PASSWORD_INPUT", "demo!")
            UiAutomatorHelper.tapText("NEXT")
            UiAutomatorHelper.waitForText("JOIN WAITING ROOM")
            TestHelper.scrollToComponent("PLAY_SOUNDS_CHECKBOX")
            TestHelper.tap("PLAY_SOUNDS_CHECKBOX")
        }
    }


    /**
     * Receive video conference call via notification
     */
    @Test
    fun tapNotification() {
        //Join the waiting room
        TestHelper.tap("JOIN_WAITING_BUTTON")
        TestHelper.screenshot("SelectDoctor")
        TestHelper.tap("NEXT_AVAILABLE_BUTTON")
        UiAutomatorHelper.waitForText("You have joined the Waiting Room")
        TestHelper.sleep(2000);

        //Save information so we can use it later to send video conference call
        val fcmToken = PreferenceHelper.loadItem(SetupHelper.mainActivity, R.string.preference_fcm_token);
        val ticketNumber = PreferenceHelper.loadItem(SetupHelper.mainActivity, R.string.preference_ticket_number);
        TestHelper.log("fcmToken: $fcmToken");
        TestHelper.log("ticketNumber: $ticketNumber");

        //Close applications
        UiAutomatorHelper.closeApps();


        //Create video conference call via API
        TestHelper.sleep(2000);
        ApiHelper.createNewSession(parseInt(ticketNumber), fcmToken);

        //Tap notification
        Thread.sleep(5000)
        tapDeclineNotification()
        UiAutomatorHelper.waitForText("You have joined the Waiting Room")
        UiAutomatorHelper.closeApps()

    }

    fun tapDeclineNotification(){
        val deviceType = UiAutomatorHelper.getDeviceType()
        TestHelper.log("Device Type: $deviceType")
        if(deviceType==DeviceTypes.SAMSUNG_PHONE)
            UiAutomatorHelper.tap(303,369)
        else if(deviceType==DeviceTypes.VIVO_PHONE)
            UiAutomatorHelper.tap(172,314)
        else if(deviceType==DeviceTypes.SAMSUNG_8_INCH_TABLET)
            UiAutomatorHelper.tap(258,144)
        else if(deviceType==DeviceTypes.SAMSUNG_LARGE_TABLET)
            UiAutomatorHelper.tap(369,171)

    }



}