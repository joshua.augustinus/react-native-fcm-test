package com.augustinus.fcmtest.util;



import java.io.IOException;

import androidx.test.runner.screenshot.BasicScreenCaptureProcessor;
import androidx.test.runner.screenshot.ScreenCapture;
import androidx.test.runner.screenshot.ScreenCaptureProcessor;

public class CustomScreenCaptureProcessor extends BasicScreenCaptureProcessor {

    @Override
    public String getFilename(String prefix){
        return prefix;
    }

}
