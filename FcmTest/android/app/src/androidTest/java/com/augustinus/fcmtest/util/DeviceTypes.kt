package com.augustinus.fcmtest.util

object DeviceTypes{
    /**
     * These map to productName inside UiAutomator
     */
    val SAMSUNG_PHONE = "j7y17ltedx"
    val VIVO_PHONE = "1904_AU"
    val SAMSUNG_8_INCH_TABLET = "gtowifixx"
    val SAMSUNG_LARGE_TABLET = "gta3xlxx"
}