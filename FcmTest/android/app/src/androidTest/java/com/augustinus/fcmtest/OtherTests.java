package com.augustinus.fcmtest;


import android.Manifest;

import com.augustinus.fcmtest.util.SetupHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.UUID;

import androidx.test.filters.LargeTest;
import androidx.test.rule.GrantPermissionRule;

import static androidx.test.espresso.action.ViewActions.click;
import static com.augustinus.fcmtest.util.TestHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.fail;


@LargeTest
public class OtherTests {

    @Rule
    public GrantPermissionRule permissionRead = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE);

    @Rule
    public GrantPermissionRule permissionWrite = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setup(){
        SetupHelper.setup();
        tap("APP_HEADER");
        tap("E2E_RESET_STATE_BUTTON");
        tap("TRUE_HOME_BUTTON");
    }

    @After
    public void cleanup(){
        SetupHelper.cleanup();
    }


    @Test
    public void loginAfterReset() {
        expect("USERNAME_INPUT");
        inputText("USERNAME_INPUT", "demo!");
        inputText("PASSWORD_INPUT", "demo!");
        tap("LOGIN_BUTTON");
        expect("SCROLLVIEW");
        screenshot("Home");
    }

    @Test
    public void clearTicketNumberOnReset(){
        tap("APP_HEADER");
        expect("TICKET_NUMBER_FIELD");
        expect("INVALID_TICKET_WARNING");
    }

    @Test
    public void haveCreateUserScreen(){
        tap("CREATE_ACCOUNT_BUTTON");
        expect("CREATE_USER_FORM");
        screenshot("CreateUser");
        String username = UUID.randomUUID().toString();
        String password = username;

        inputText("USERNAME_INPUT","CREATE_USER_FORM", username);
        inputText("PASSWORD_INPUT", "CREATE_USER_FORM",password);
        tap("LOGIN_BUTTON","CREATE_USER_FORM" );
        screenshot("ChangeDisplayName");
        tap("SET_DISPLAY_NAME_BUTTON");
        expect("JOIN_WAITING_BUTTON");
    }




}