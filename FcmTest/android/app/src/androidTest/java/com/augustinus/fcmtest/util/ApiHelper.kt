package com.augustinus.fcmtest.util

import org.json.JSONObject
import java.io.DataOutputStream
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL


class ApiHelper {
    //Use companion object to do static functions
    companion object {
        /**
         * Creates a new video conferencing session. Likely a notification will popup on the test device
         */
        fun createNewSession(ticketNumber: Int, token: String) {
            if(ticketNumber<=0){
                throw IllegalArgumentException("ticketNumber must be greater than zero");
            }

            if(token.length==0){
                throw IllegalArgumentException("token must be not empty");
            }
            TestHelper.log("postRequest called");
            val jsonParam = JSONObject()
            jsonParam.put("ticketNumber", ticketNumber);
            jsonParam.put("registrationToken", token);

            val url = "https://staging-fcm-api.azurewebsites.net/session";
            createNewSession(url, jsonParam);

        }

        /**
         * Make a POST request to a REST api
         * Does assert fail if there is exception
         */
        fun createNewSession(url:String, jsonPayload:JSONObject){
            //Probably want to change this
            //https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/issues/249
            val authToken = SetupHelper.getAccessToken();

            val url = URL(url);
            try{
                with(url.openConnection() as HttpURLConnection) {
                    // optional default is GET
                    requestMethod = "POST"
                    setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    setRequestProperty("Accept", "*/*");
                    setRequestProperty("Authorization", "Bearer $authToken" );

                    setDoOutput(true);

                    // Send POST output.
                    val printout = DataOutputStream(getOutputStream());
                    printout.writeBytes(jsonPayload.toString());
                    printout.flush ();
                    printout.close ();


                    val responseCode: Int = getResponseCode();
                    TestHelper.log("Response code: " + responseCode);

                    if(responseCode!=200){
                        TestHelper.assertFail("Response code was not 200");
                    }

                }
            }catch (ex:Exception){
                TestHelper.log(ex.message);
                TestHelper.assertFail(ex.message);
            }
        }
    }
}