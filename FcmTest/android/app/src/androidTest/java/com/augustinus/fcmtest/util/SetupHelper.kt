package com.augustinus.fcmtest.util

import android.app.Activity
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ActivityScenario.ActivityAction
import com.augustinus.fcmtest.MainActivity
import com.augustinus.fcmtest.PreferenceHelper

object SetupHelper {
    private var isFirstLoad = true
    private var activityScenario: ActivityScenario<MainActivity>? = null
    lateinit var mainActivity: Activity
    @JvmStatic
    fun setup() {
        activityScenario = ActivityScenario.launch(MainActivity::class.java)
        UiAutomatorHelper.waitForText("Demo Project")

        activityScenario!!.onActivity(ActivityAction { activity: Activity ->
            mainActivity = activity;

        })
    }

    /**
     * Should be called inside @After
     */
    @JvmStatic
    fun cleanup() {
        TestHelper.sleep(1000)
    }

    @JvmStatic
    fun screenshot(name: String?) {
        TestHelper.sleep(2000)
        ScreenShotHelper.firebaseCapture(mainActivity, name)
    }

    fun getAccessToken() : String?{
        val preference = PreferenceHelper.getPreferenceFile(mainActivity);
        val defaultValue = null;
        val accessToken = preference.getString("AccessToken",defaultValue);
        return accessToken;
    }

}