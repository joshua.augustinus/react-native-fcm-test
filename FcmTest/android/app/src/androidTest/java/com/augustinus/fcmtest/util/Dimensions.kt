package com.augustinus.fcmtest.util

class Dimensions
{
    var width:Int = 0
    var height:Int = 0

    constructor(width:Int, height:Int){
        this.width = width
        this.height=height
    }

}
