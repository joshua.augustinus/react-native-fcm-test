package com.augustinus.fcmtest;


import android.Manifest;

import com.augustinus.fcmtest.util.ApiHelper;
import com.augustinus.fcmtest.util.SetupHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import androidx.test.espresso.Espresso;
import androidx.test.filters.LargeTest;
import androidx.test.rule.GrantPermissionRule;


import static com.augustinus.fcmtest.util.TestHelper.*;
import static com.augustinus.fcmtest.util.TestHelper.scrollToComponent;
import static org.junit.Assert.fail;

@RunWith(JUnit4.class)
@LargeTest
public class LoggedInTests {

    @Rule
    public GrantPermissionRule permissionCamera = GrantPermissionRule.grant(Manifest.permission.CAMERA);
    @Rule
    public GrantPermissionRule permissionAudio = GrantPermissionRule.grant(Manifest.permission.RECORD_AUDIO);
    @Rule
    public GrantPermissionRule permissionLocation = GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION);
    @Rule
    public GrantPermissionRule permissionRead = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE);
    @Rule
    public GrantPermissionRule permissionWrite = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @BeforeClass
    public static void setup(){
        SetupHelper.setup();
        tap("APP_HEADER");
        tap("E2E_RESET_STATE_BUTTON");
        tap("TRUE_HOME_BUTTON");
        expect("USERNAME_INPUT");
        inputText("USERNAME_INPUT", "demo!");
        inputText("PASSWORD_INPUT", "demo!");
        tap("LOGIN_BUTTON");
        expect("JOIN_WAITING_BUTTON");
        scrollToComponent("PLAY_SOUNDS_CHECKBOX");
        tap("PLAY_SOUNDS_CHECKBOX");
        scrollToComponent("JOIN_WAITING_BUTTON");
    }

    @Before
    public void beforeEach(){
        SetupHelper.setup();
    }

    @After
    public void cleanup(){

        SetupHelper.cleanup();

    }

    @Test
    public void navigateToWaitingRoom() {

        tap("JOIN_WAITING_BUTTON");
        screenshot("SelectDoctor");
        tap("NEXT_AVAILABLE_BUTTON");
        tap("CANCEL_APPOINTMENT_BUTTON");
    }

    @Test public void canGoToWaitingRoomFromE2e(){
        tap("JOIN_WAITING_BUTTON");
        tap("NEXT_AVAILABLE_BUTTON");
        expect("CANCEL_APPOINTMENT_BUTTON");
        sleep(2000);
        tap("APP_HEADER");
        inputText("Q_POSITION_INPUT", "1");

        tap("E2E_WAITING_BUTTON");
        tap("CANCEL_APPOINTMENT_BUTTON");
    }

    @Test
    public void havePicturesInWaitingRoom(){
        tap("JOIN_WAITING_BUTTON");
        tap("NEXT_AVAILABLE_BUTTON");
        expect("CANCEL_APPOINTMENT_BUTTON");

        for(int i=1;i<=12;i++){
            sleep(2000);
            tap("APP_HEADER");
            log("Attempting q position: "+i);
            inputText("Q_POSITION_INPUT", Integer.toString(i));

            tap("E2E_WAITING_BUTTON");

            screenshot("Waiting_Q"+Integer.toString(i));


        }

        tap("CANCEL_APPOINTMENT_BUTTON");
        expect("JOIN_WAITING_BUTTON");
    }

    /**
     * It should start at the Waiting Room if you have a ticket on restarting the app
     */
    @Test
    public void startAtWaitingRoom(){
        tap("JOIN_WAITING_BUTTON");
        tap("NEXT_AVAILABLE_BUTTON");
        expect("CANCEL_APPOINTMENT_BUTTON");
        sleep(4000);
        SetupHelper.setup();

        tap("CANCEL_APPOINTMENT_BUTTON");

    }

    @Test
    public void haveChangePasswordScreen(){
        tap("CHANGE_PASSWORD_BUTTON");
        screenshot("ChangePassword");
    }

    @Test
    public void displayNameUpdatesToChanges(){
        tap("CHANGE_DISPLAY_NAME_BUTTON");
        String displayName = getText("DISPLAY_NAME_INPUT");
        log("displayName: " + displayName);
        tap("APP_HEADER");

        String displayName2 = getText("DISPLAY_NAME");
        log("displayName2: " + displayName2);
        if(!displayName.equalsIgnoreCase(displayName2)){
            fail("Display name does not match");
        }
    }

    @Test
    public void haveMedicareScreen() {
        scrollToComponent("IS_CARE_PROVIDER_CHECKBOX");
        tap("IS_CARE_PROVIDER_CHECKBOX");
        tap("JOIN_WAITING_BUTTON");
        expect("MEDICARE_CARD");
        screenshot("MedicareSwipe");
        Espresso.pressBack();
        scrollToComponent("IS_CARE_PROVIDER_CHECKBOX");
        tap("IS_CARE_PROVIDER_CHECKBOX");
    }

}