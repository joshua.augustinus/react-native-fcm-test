package com.augustinus.fcmtest.util

import android.app.NotificationManager
import android.content.Context

object NotificationHelper {
    /**
     * Gets the number of notifications alive and then dimisses them
     */
    fun dismissNotifications() : Int{
        val context = SetupHelper.mainActivity;
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notifications = notificationManager.activeNotifications;
        var numberNotifications = 0;
        for (notification in notifications) {
            val notificationId = notification.id;
            TestHelper.log("Past notification id: $notificationId");
            notificationManager.cancel(notificationId)
            numberNotifications++
        }

        return numberNotifications;
    }
}