package com.augustinus.fcmtest;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.Map;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class CustomFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "ReactNative";
    public static final String EXTRA_MESSAGE = "ExtraMessage";
    public static final String EXTRA_MAP = "ExtraMap";
    private static final String LAUNCH_METHOD = "launchMethod";
    private static final String TICKET_NUMBER = "ticketNumber";

    public CustomFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> data = remoteMessage.getData();

            String event = data.get("event");
            Log.d(TAG, "Event: " + event);
            if(event.equalsIgnoreCase("createSession")){
                String ticketNumber = data.get("ticketNumber");
                Log.d(TAG, "Attempting notification...");
                    showNotification(remoteMessage, ticketNumber);
                    Log.d(TAG, "Finished notification");


            }
            else if(event.equalsIgnoreCase("endSession")){
                Log.d(TAG, "endSession event");
                GlobalState.getInstance().clearPreviousNotificationIds(this);
                SoundHelper.stopCallSound();
            }
        }
    }

    private boolean isLocked(){
        Context context = getApplicationContext();
        KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        if( myKM.inKeyguardRestrictedInputMode()) {
            return true;
        } else {
           return false;
        }

    }


    private String loadSharedPreference(String key, String defaultValue) {
        Context context = getApplicationContext();
        SharedPreferences sharedPref = PreferenceHelper.getPreferenceFile(context);

        String result = sharedPref.getString(key, defaultValue);
        return result;
    }

    private void showVideoCallIncoming(RemoteMessage remoteMessage) {
        Log.d(TAG, "showVideoCallIncoming");
        Intent i = new Intent(getBaseContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra(EXTRA_MESSAGE, remoteMessage);
        startActivity(i);
    }

    /**
     * Request code is used to identify the intent. Choose a unique number
     *
     * @param requestCode a unique number
     * @return
     */
    private PendingIntent createDeclineCallIntent(int requestCode, String ticketNumber) {
        Intent i = new Intent(getBaseContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        //Create hashmap of extra stuff
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(LAUNCH_METHOD, "declineCall");
        map.put(TICKET_NUMBER, ticketNumber);
        i.putExtra(EXTRA_MAP, map);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, i, PendingIntent.FLAG_CANCEL_CURRENT);
        return pendingIntent;
    }

    private PendingIntent createAcceptCallIntent(RemoteMessage remoteMessage, int requestCode) {
        String launchMethod = "notification";
        if(isLocked()){
            launchMethod = "lockScreen";
        }
        // Create an explicit intent for an Activity in your app
        Intent i = new Intent(getBaseContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra(EXTRA_MESSAGE, remoteMessage);

        //Create hashmap of extra stuff
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(LAUNCH_METHOD, launchMethod);
        i.putExtra(EXTRA_MAP, map);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, i, PendingIntent.FLAG_CANCEL_CURRENT);
        return pendingIntent;
    }

    private void showNotification(RemoteMessage remoteMessage, String ticketNumber) {
        try {
            boolean isLocked = isLocked();
            Log.d(TAG,"Is locked: " + isLocked);
            int acceptCallRequestCode = 0;
            int declineCallRequestCode = 1;
            String channelId = getString(R.string.channel_id);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.drawable.ic_action_name)
                    .setContentTitle("Incoming Video Conference Call")
                    .setContentText("From Demo Doctor")
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_CALL)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setFullScreenIntent(createAcceptCallIntent(remoteMessage, acceptCallRequestCode), true);


            if(!isLocked){
                builder.addAction(R.drawable.ic_action_name, "DECLINE", createDeclineCallIntent(acceptCallRequestCode, ticketNumber))
                        .addAction(R.drawable.ic_stat_call, "ACCEPT", createAcceptCallIntent(remoteMessage, declineCallRequestCode));
                SoundHelper.playCallSound(this);
            }

            int notificationId = GlobalState.getInstance().getNewNotificationId();
            Log.d(TAG, "NotificationId: " + notificationId);
            //https://developer.android.com/training/notify-user/time-sensitive
            // Provide a unique integer for the "notificationId" of each notification.
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(notificationId, builder.build());



        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }


    }


}
