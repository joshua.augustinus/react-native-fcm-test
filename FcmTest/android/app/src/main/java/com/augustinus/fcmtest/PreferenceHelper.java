package com.augustinus.fcmtest;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.react.bridge.Promise;

public class PreferenceHelper {
    private  static  final String DEFAULT_PREF_FILENAME  = "MainActivity";

    public static SharedPreferences getPreferenceFile(Context context){
        SharedPreferences sharedPref =context.getSharedPreferences(DEFAULT_PREF_FILENAME,Context.MODE_PRIVATE);


        return sharedPref;
    }

    /**
     * Save shared preference using background thread
     * @param context
     * @param keyResId the R.string.id from the strings xml
     * @param value value to save
     */
    public static void saveItem(Context context, int keyResId, String value){
        //Save in preferences
        SharedPreferences sharedPref = PreferenceHelper.getPreferenceFile(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(keyResId), value);
        editor.apply();
    }

    public static String loadItem(Context context, int resId){
        SharedPreferences sharedPref =PreferenceHelper.getPreferenceFile(context);

        String result = sharedPref.getString(context.getString(resId), null);
        return result;
    }


}
