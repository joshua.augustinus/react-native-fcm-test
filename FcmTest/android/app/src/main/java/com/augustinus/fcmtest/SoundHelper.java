package com.augustinus.fcmtest;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.util.Log;

import com.facebook.react.bridge.ReactMethod;

public class SoundHelper {
    private static MediaPlayer mediaPlayer;

    public static void playCallSound(Context context){

        if(shouldPlaySound(context)){
            Log.d(MainActivity.TAG,"Starting media");
            mediaPlayer = MediaPlayer.create(context,R.raw.videocall);
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }
    }

    public static boolean shouldPlaySound(Context context){
        SharedPreferences sharedPref =PreferenceHelper.getPreferenceFile(context);

        String result = sharedPref.getString(BridgeModule.OPTION_PLAY_SOUNDS, "true");
        Log.d(MainActivity.TAG,"Play sounds option: " +result);
        return(result.equalsIgnoreCase("true"));
    }


    public static void stopCallSound() {
        if(mediaPlayer!=null)
            mediaPlayer.stop();
    }
}
