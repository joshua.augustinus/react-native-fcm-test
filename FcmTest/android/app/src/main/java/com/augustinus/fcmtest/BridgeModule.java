package com.augustinus.fcmtest;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BridgeModule extends ReactContextBaseJavaModule {
    private ReactApplicationContext reactContext;
    public static final String OPTION_PLAY_SOUNDS = "PlaySounds";
    public static final String USERNAME = "Username";
    public static final String ACCESS_TOKEN = "AccessToken";

    public static final String DISPLAY_NAME = "DisplayName";
    public static final String IS_CARE_PROVIDER = "IsCareProvider";
    private MediaPlayer mediaPlayer;

    BridgeModule(ReactApplicationContext context){
        super(context);
        reactContext = context;
    }

    @NonNull
    @Override
    public String getName() {
        return "BridgeModule";
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put("OPTION_PLAY_SOUNDS", OPTION_PLAY_SOUNDS);
        constants.put("USERNAME", USERNAME);
        constants.put("ACCESS_TOKEN", ACCESS_TOKEN);
        constants.put("TICKET_NUMBER", reactContext.getString(R.string.preference_ticket_number));
        constants.put("DISPLAY_NAME", DISPLAY_NAME);
        constants.put("IS_CARE_PROVIDER", IS_CARE_PROVIDER);
        return constants;
    }

    @ReactMethod
    public void saveSharedPreference(String key, String value, boolean useBackgroundThread, Promise promise){
        //Save in preferences
        Activity context = getCurrentActivity();
        SharedPreferences sharedPref = PreferenceHelper.getPreferenceFile(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        if(useBackgroundThread)
            editor.apply();
        else
            editor.commit();

        promise.resolve(null);
    }

    @ReactMethod
    public  void loadSharedPreference(String key, String defaultValue, Promise promise){
        Activity context = getCurrentActivity();
        SharedPreferences sharedPref =PreferenceHelper.getPreferenceFile(context);
     
        String result = sharedPref.getString(key, defaultValue);

        promise.resolve(result);
    }

    @ReactMethod
    public void removeSharedPreferences(ReadableArray keysToDelete, Promise promise )
    {
        Activity context = getCurrentActivity();
        SharedPreferences sharedPref =PreferenceHelper.getPreferenceFile(context);
        SharedPreferences.Editor editor = sharedPref.edit();

        for(int i =0;i<keysToDelete.size();i++){
            String key = keysToDelete.getString(i);
            Log.d(MainActivity.TAG, key);
            editor.remove(key);
        }


        editor.commit();

        promise.resolve(null);
    }

    @ReactMethod
    public void deleteSharedPreferences(Promise promise){
        Activity context = getCurrentActivity();
        SharedPreferences sharedPref =PreferenceHelper.getPreferenceFile(context);
        sharedPref.edit().clear().commit();
        promise.resolve(null);
    }


    /**
     * Obsolete
     */
    @ReactMethod void playCallSound(){
        //Check preferences first
        Activity context = getCurrentActivity();
        SoundHelper.playCallSound(context);
    }

    /**
     * Obsolete
     */
    @ReactMethod void stopCallSound() {
        if(mediaPlayer!=null)
            mediaPlayer.stop();
    }

    @ReactMethod void playNotificationSound(){

        Activity context = getCurrentActivity();
        if(SoundHelper.shouldPlaySound(context)){
            mediaPlayer = MediaPlayer.create(context,R.raw.blop);
            mediaPlayer.start();
        }

    }

    @ReactMethod void playJoinWaitingSound(){
        Activity context = getCurrentActivity();
        if(SoundHelper.shouldPlaySound(context)){
            mediaPlayer = MediaPlayer.create(context,R.raw.waiting_enter);
            mediaPlayer.start();
        }

    }




    }



