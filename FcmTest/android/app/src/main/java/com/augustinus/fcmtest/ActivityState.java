package com.augustinus.fcmtest;

import java.util.UUID;

public class ActivityState {
    private static ActivityState sharedInstance = new ActivityState();
    private UUID uniqueId;
    public boolean isActive;

    private ActivityState() {
    }//prevent instances

    public static ActivityState getInstance() {
        return sharedInstance;
    }

    public void onCreate(UUID uniqueId){
        isActive = true;
        this.uniqueId = uniqueId;
    }

    public void onResume(UUID uniqueId){
        isActive = true;
    }


    public void onDestroy(UUID uniqueId){
        if(uniqueId==this.uniqueId){
            isActive=false;
        }

    }

}
