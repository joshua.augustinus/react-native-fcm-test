package com.augustinus.fcmtest;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import androidx.annotation.NonNull;

//Copied from https://reactnative.dev/docs/native-modules-android
public class CustomPackage implements  ReactPackage {

    @NonNull
    @Override
    public List<NativeModule> createNativeModules(@NonNull ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();

        modules.add(new BridgeModule(reactContext));
        modules.add(new FcmBridge(reactContext));
        GlobalState.getInstance().setReactionApplicationContext(reactContext);
        return modules;
    }

    @NonNull
    @Override
    public List<ViewManager> createViewManagers(@NonNull ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
}
