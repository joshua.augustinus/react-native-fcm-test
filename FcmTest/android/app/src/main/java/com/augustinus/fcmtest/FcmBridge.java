package com.augustinus.fcmtest;

import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;

import androidx.annotation.NonNull;

public class FcmBridge extends ReactContextBaseJavaModule  {

    private ReactApplicationContext reactContext;

    FcmBridge(ReactApplicationContext context){
        super(context);
        reactContext = context;
    }

    @NonNull
    @Override
    public String getName() {
        return "FcmBridge";
    }

    /**
     * Deprecated
     * Don't use this method as it deletes the registrationToken
     * Removes subscription from token then subscribe to topic
     */
    @ReactMethod
    public void subscribeToTopic(String topic, Promise promise ){
        Log.d(MainActivity.TAG, "Inside Fcm.subscribeToTopic");

                //Unsubscribe to everything
                try{
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    Log.d(MainActivity.TAG, "Unsubscribed all topics");
                }  catch(IOException e){
                    Log.d(MainActivity.TAG,   e.getMessage());
                }

                //Subscribe to topic
                FirebaseMessaging.getInstance().subscribeToTopic(topic)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                String msg;
                                if (!task.isSuccessful()) {
                                    msg = "Failed to subscribe to topic: " + topic;
                                }else{
                                    msg = "Subscribed to topic: "+topic;

                                }

                                Log.d(MainActivity.TAG, msg);
                            }
                        });



    }

    @ReactMethod
    public void getRegistrationToken(Promise promise) {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(MainActivity.TAG, "getInstanceId failed", task.getException());
                            promise.reject(task.getException());
                            return;
                        } else {
                            // Get new Instance ID token
                            String token = task.getResult().getToken();

                            // Log and toast
                            String msg = "Registration token: " + token;
                            Log.d(MainActivity.TAG, msg);
                            promise.resolve(token);

                            PreferenceHelper.saveItem(reactContext,R.string.preference_fcm_token,token);
                        }


                    }
                });
    }

    @ReactMethod
    public void clearNotifications(){
        GlobalState.getInstance().clearPreviousNotificationIds(reactContext);
        SoundHelper.stopCallSound();
    }


}
