package com.augustinus.fcmtest;

import android.app.KeyguardManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.facebook.react.ReactActivity;

import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import android.view.WindowManager;

import com.facebook.react.ReactActivityDelegate;
import android.util.Log;

import org.devio.rn.splashscreen.SplashScreen;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.test.espresso.IdlingResource;

public class MainActivity extends ReactActivity {
  private Bundle propsBundle = new Bundle();
  public static final String TAG = "ReactNative";
  private boolean isSubscribed = false;
  private UUID uniqueId;
  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "FcmTest";
  }

  @Override
  public void onNewIntent(Intent intent) {
    setIntent(intent);
    super.onNewIntent(intent);
  }


  @Override
  public void onResume() {
    super.onResume();
    Log.d(TAG,"MainActivity: onResume " + uniqueId);
    ActivityState.getInstance().onResume(this.uniqueId);
  }

  @Override
  public void onDestroy() {
    super.onDestroy();

    Log.d(TAG,"MainActivity: onDestroy " + uniqueId);
    ActivityState.getInstance().onDestroy(this.uniqueId);
  }

  @Override
    protected void onCreate(Bundle savedInstanceState) {
    uniqueId = UUID.randomUUID();
    ActivityState.getInstance().onCreate(uniqueId);
    Log.d(TAG,"MainActivity: onCreate " + uniqueId);
    SplashScreen.show(this);

    showOverLockScreen();
    
    //logToken(); //Only for debug


    //Check if Activity was launched by FirebaseMessagingService
    //If yes, then add props
    //This must be called before super.onCreate
    Bundle bundle = this.getIntent().getExtras();
    if(bundle!=null  ){
      SoundHelper.stopCallSound();
      clearNotifications();
      if(bundle.containsKey(CustomFirebaseMessagingService.EXTRA_MESSAGE)){
        Log.d(TAG, "MainActivity has extra message");
        RemoteMessage remoteMessage = (RemoteMessage)this.getIntent().getParcelableExtra(CustomFirebaseMessagingService.EXTRA_MESSAGE);
        Map<String, String> map = remoteMessage.getData();
        for (Map.Entry<String, String> entry : map.entrySet()) {
          propsBundle.putString(entry.getKey(), entry.getValue());

        }
      }

      if(bundle.containsKey(CustomFirebaseMessagingService.EXTRA_MAP)){
        Log.d(TAG, "MainActivity has extra map");
        HashMap<String,String> hashMap = (HashMap<String,String>)this.getIntent().getSerializableExtra((CustomFirebaseMessagingService.EXTRA_MAP));
        for(Map.Entry<String,String> entry:hashMap.entrySet()){
          propsBundle.putString(entry.getKey(), entry.getValue());
        }
      }

    }else{
      Log.d(TAG, "MainActivity doesn't have extra message");
    }
    super.onCreate(savedInstanceState);
    createNotificationChannel();


}

private void clearNotifications(){

  GlobalState.getInstance().clearPreviousNotificationIds(this);

}
  ///https://stackoverflow.com/questions/35356848/android-how-to-launch-activity-over-lock-screen
  private void showOverLockScreen(){

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1)
    {
      setShowWhenLocked(true);
      setTurnScreenOn(true);
      KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
      if(keyguardManager!=null)
        keyguardManager.requestDismissKeyguard(this, null);
    }
    else
    {
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
              WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
              WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

  }




  @Override
  protected ReactActivityDelegate createReactActivityDelegate() {
    Log.d(TAG, "Inside createReactActivityDelegate");
    return new ReactActivityDelegate(this, getMainComponentName()) {

      @Override
      protected Bundle getLaunchOptions() {
        return propsBundle;
      }
    };
  }

  private void createNotificationChannel() {
    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      String channelId = getString(R.string.channel_id);
      CharSequence name = getString(R.string.channel_name);
      String description = getString(R.string.channel_description);
      int importance = NotificationManager.IMPORTANCE_HIGH;
      NotificationChannel channel = new NotificationChannel(channelId, name, importance);
      channel.setDescription(description);
      channel.setSound(null, null);
      // Register the channel with the system; you can't change the importance
      // or other notification behaviors after this
      NotificationManager notificationManager = getSystemService(NotificationManager.class);
      notificationManager.createNotificationChannel(channel);
      Log.d(MainActivity.TAG, "Created notification channel");
    }
  }


}
