package com.augustinus.fcmtest;

import android.app.NotificationManager;
import android.content.Context;
import android.provider.Settings;

import com.facebook.react.bridge.ReactApplicationContext;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.List;

import androidx.test.espresso.IdlingResource;

public class GlobalState {
    private static GlobalState sharedInstance = new GlobalState();
    private ReactApplicationContext reactApplicationContext;
    private int notificationId = 0;
    private List<Integer> previousNotificationIds = new ArrayList<Integer>();


    private GlobalState() {
    }//prevent instances

    public static GlobalState getInstance() {
        return sharedInstance;
    }

    public ReactApplicationContext getReactContext(){
        return reactApplicationContext;
    }


    public  void setReactionApplicationContext(ReactApplicationContext context){
        reactApplicationContext = context;
    }

    public int getNewNotificationId(){
        notificationId++;
        previousNotificationIds.add(notificationId);
        return notificationId;
    }

    public List<Integer> getPreviousNotificationIds(){
        return previousNotificationIds;
    }

    public void clearPreviousNotificationIds(Context context){
        List<Integer> ids = GlobalState.getInstance().getPreviousNotificationIds();
        NotificationManager nMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        for(int notificationId : ids){

            nMgr.cancel(notificationId);
        }

        previousNotificationIds = new ArrayList<Integer>();
    }


}
