#https://medium.com/idopterlabs/working-with-environment-variables-in-appcenter-aba44716eda0
#> Means output to file
#>> Means append to file
cd ${APPCENTER_SOURCE_DIRECTORY}
echo "SIGNALR_URL=${SIGNALR_URL}" > .env
echo "FCM_URL=${FCM_URL}" >> .env
echo "USER_URL=${USER_URL}" >> .env
echo "ENVIRONMENT=${ENVIRONMENT}" >> .env