module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        cwd: 'babelrc',
        extensions: ['.ts', '.tsx', '.js', '.ios.js', '.android.js'],
        alias: {
          '@screens': './src/screens',
          '@components': './src/components',
          '@util': './src/util',
          '@src': './src',
          '@e2e': './e2e',
        },
      },
    ],
  ],
};
