import React, {useEffect} from 'react';
import {AppState, AppStateStatus, SafeAreaView, StyleSheet} from 'react-native';
import BridgeModule from './nativemodules/BridgeModule';
import VideoConferenceIncoming from '@screens/VideoConferenceIncoming';
import CreateUser from '@screens/CreateUser';
import {FcmProps, RootStackParamList} from './types';
import {createStackNavigator} from '@react-navigation/stack';
import HiddenScreen from '@screens/HiddenScreen';
import WaitingRoom from '@screens/WaitingRoom';
import TestHome from '@screens/TestHome';
import Config from 'react-native-config';
import JoinWaitingRoom from '@screens/JoinWaitingRoom';
import UserRegistered from '@screens/UserRegistered';
import ChangePassword from '@screens/ChangePassword';
import AppHeader from './components/AppHeader';
import MedicareSwipe from '@screens/MedicareSwipe';
import {BluetoothTest} from '@screens/BluetoothTest';

const Stack = createStackNavigator<RootStackParamList>();
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

async function handleAppStateChange(nextAppState: AppStateStatus) {
  if (nextAppState.match(/inactive|background/)) {
    await BridgeModule.saveAllSharedPreferences();
    console.log('Saved all preferences');
  }
}

const App = (props: FcmProps) => {
  console.log('env check', Config.ENVIRONMENT);
  console.log('Launch Method:', props.launchMethod);
  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);

    return () => {
      console.log('App unmounted');

      AppState.removeEventListener('change', handleAppStateChange);
    };
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <AppHeader />

      <Stack.Navigator
        initialRouteName="HiddenScreen"
        screenOptions={{headerShown: false}}>
        <Stack.Screen
          name="HiddenScreen"
          component={HiddenScreen}
          initialParams={{
            sessionId: props.sessionId,
            apiKey: props.apiKey,
            token: props.token,
            launchMethod: props.launchMethod,
            ticketNumber: props.ticketNumber,
          }}
        />
        <Stack.Screen
          name="VideoConferenceIncoming"
          component={VideoConferenceIncoming}
        />
        <Stack.Screen name="CreateUser" component={CreateUser} />
        <Stack.Screen name="WaitingRoom" component={WaitingRoom} />
        <Stack.Screen name="JoinWaitingRoom" component={JoinWaitingRoom} />
        <Stack.Screen name="TestHome" component={TestHome} />
        <Stack.Screen name="UserRegistered" component={UserRegistered} />
        <Stack.Screen name="ChangePassword" component={ChangePassword} />
        <Stack.Screen name="MedicareSwipe" component={MedicareSwipe} />
        <Stack.Screen name="BluetoothTest" component={BluetoothTest} />
      </Stack.Navigator>
    </SafeAreaView>
  );
};

export default App;
