import {NativeModules} from 'react-native';
import store from '../store';
import {SavedRootState} from '../types';

const NBridgeModule = NativeModules.BridgeModule;

const BridgeModule = {
  /**
   * At the moment the call to the native method is not blocking. We don't know when it finishes
   * Possibly we could use promises to figure it out
   * @param useBackgroundThread determines whether to use editor.commit or editor.apply
   */
  saveAllSharedPreferences: async function (
    useBackgroundThread: boolean = false,
  ) {
    let state = store.getState();
    let username = state.username;
    let access_token = state.access_token;
    let playSounds = state.options.playSounds;
    let isCareProvider = state.options.isCareProvider;

    await this.saveSharedPreference(
      NBridgeModule.USERNAME,
      username,
      useBackgroundThread,
    );
    await this.saveSharedPreference(
      NBridgeModule.ACCESS_TOKEN,
      access_token,
      useBackgroundThread,
    );
    await this.saveSharedPreference(
      NBridgeModule.OPTION_PLAY_SOUNDS,
      playSounds,
      useBackgroundThread,
    );
    await this.saveSharedPreference(
      NBridgeModule.IS_CARE_PROVIDER,
      isCareProvider,
      useBackgroundThread,
    );
    await this.saveSharedPreference(
      NBridgeModule.DISPLAY_NAME,
      state.displayName,
      useBackgroundThread,
    );
  },
  saveTicketNumber: async function (ticketNumber: number) {
    let useBackgroundThread = true;
    await this.saveSharedPreference(
      NBridgeModule.TICKET_NUMBER,
      ticketNumber.toString(),
      useBackgroundThread,
    );
  },

  saveSharedPreference: async function (
    key: string,
    value: any,
    useBackgroundThread: boolean = false,
  ) {
    console.log('saving shared preference: ', key, value);

    if (typeof value === 'boolean')
      await NBridgeModule.saveSharedPreference(
        key,
        value.toString(),
        useBackgroundThread,
      );
    else
      await NBridgeModule.saveSharedPreference(key, value, useBackgroundThread);
  },

  loadSharedPreference: function (key: string, defaultValue: string) {
    return NBridgeModule.loadSharedPreference(key, defaultValue);
  },

  loadAllSharedPreferences: async function (): Promise<SavedRootState> {
    let username = await this.loadSharedPreference(
      NBridgeModule.USERNAME,
      null,
    );
    let access_token = await this.loadSharedPreference(
      NBridgeModule.ACCESS_TOKEN,
      null,
    );

    //This will be a string
    let playSounds = await this.loadSharedPreference(
      NBridgeModule.OPTION_PLAY_SOUNDS,
      'true',
    );

    let isCareProvider = await this.loadSharedPreference(
      NBridgeModule.IS_CARE_PROVIDER,
      'false',
    );

    let ticketNumber = await this.loadSharedPreference(
      NBridgeModule.TICKET_NUMBER,
      '0',
    );

    let displayName = await this.loadSharedPreference(
      NBridgeModule.DISPLAY_NAME,
      null,
    );

    return {
      username: username,
      access_token: access_token,
      options: {
        playSounds: playSounds === 'true',
        isCareProvider: isCareProvider === 'true',
      },
      ticketNumber: parseInt(ticketNumber),
      displayName: displayName,
    };
  },

  /**
   * Deletes the shared preferences file then returns RootState with default values
   */
  resetSharedPreference: async function () {
    await NBridgeModule.deleteSharedPreferences();
    return await this.loadAllSharedPreferences();
  },

  playCallSound: function () {
    console.log('Calling BridgeModule.playCallSound()');
    NBridgeModule.playCallSound();
  },

  stopCallSound: function () {
    NBridgeModule.stopCallSound();
  },

  playNotificationSound: function () {
    NBridgeModule.playNotificationSound();
  },

  playJoinWaitingSound: function () {
    NBridgeModule.playJoinWaitingSound();
  },
};

export default BridgeModule;
