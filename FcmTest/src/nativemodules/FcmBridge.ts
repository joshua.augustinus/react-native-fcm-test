import {NativeModules} from 'react-native';
const _FcmBridge = NativeModules.FcmBridge;

const FcmBridge = {
  /**
   * Deprecated
   * When communicating to the device using FCM we use registrationToken instead of topic
   * @param topic
   */
  subscribeToTopic: function (topic: string) {
    /**
     * https://reactnative.dev/docs/native-modules-ios
     * The native module should not have any assumptions about what thread it is being called on
     */
    _FcmBridge.subscribeToTopic(topic);
  },

  getRegistrationToken: async function () {
    return await _FcmBridge.getRegistrationToken();
  },

  clearNotifications: function () {
    _FcmBridge.clearNotifications();
  },
};

export {FcmBridge};
