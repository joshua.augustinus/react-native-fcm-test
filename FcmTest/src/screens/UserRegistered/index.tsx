import React, {useEffect} from 'react';
import {Avatar, Card, Title, Paragraph, useTheme} from 'react-native-paper';
import styles from './styles';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {RootState} from '../../types';
import BridgeModule from '../../nativemodules/BridgeModule';
import BusyButton from '@components/BusyButton';
import testIDs from '@e2e/testIDs';
import * as Animatable from 'react-native-animatable';
import {ScrollView} from 'react-native-gesture-handler';
import Settings from '@src/components/Settings';
import {useScreenInfo} from '@util/customHooks';
import VideoConferenceGate from '@components/VideoConferenceGate';

const homeImg = require('../../../assets/images/home.png');

const LeftContent = (props: any) => <Avatar.Icon {...props} icon="account" />;

const UserRegistered = () => {
  const navigation = useNavigation();
  const {colors} = useTheme();
  const screenInfo = useScreenInfo();
  const isSmallScreenSize = screenInfo.isSmallScreen;
  const windowWidth = screenInfo.width;
  const displayName = useSelector((state: RootState) => state.displayName);
  const isCareProvider = useSelector(
    (state: RootState) => state.options.isCareProvider,
  );

  const imgStyle = {width: '100%', height: Math.min(250, windowWidth / 2)};

  const changePasswordHandler = () => {
    navigation.navigate('ChangePassword');
  };

  const joinWaitingRoomHandler = () => {
    if (isCareProvider) {
      navigation.navigate('MedicareSwipe');
    } else {
      navigation.navigate('JoinWaitingRoom');
    }
  };

  const changeDisplayNameHandler = () => {
    navigation.navigate('CreateUser', {screen: 'ChangeDisplayName'});
  };

  const getTitle = () => {
    if (isSmallScreenSize) {
      return displayName;
    } else {
      return (
        <Title>
          App is registered to{' '}
          <Title style={{color: colors.placeholder}}>{displayName}</Title>
        </Title>
      );
    }
  };

  const bluetoothHandler = () => {
    navigation.navigate('BluetoothTest');
  };

  useEffect(() => {
    BridgeModule.saveAllSharedPreferences(true);
  }, []);

  return (
    <>
      <ScrollView testID={testIDs.SCROLLVIEW}>
        <Card style={styles.card}>
          <Card.Title
            titleStyle={{flexWrap: 'wrap', flexDirection: 'row'}}
            title={getTitle()}
            subtitle=""
            left={LeftContent}
          />
          <Card.Content>
            <Paragraph>
              Welcome. You can click Join Waiting Room to video conference with
              a doctor.
            </Paragraph>

            <Animatable.Image
              delay={500}
              animation="fadeInDownBig"
              duration={1000}
              source={homeImg}
              style={imgStyle}
              resizeMode="contain"
            />
          </Card.Content>
          <Card.Actions style={styles.buttonContainer}>
            <BusyButton
              testID={testIDs.JOIN_WAITING_BUTTON}
              onPress={joinWaitingRoomHandler}>
              Join Waiting Room
            </BusyButton>
            <BusyButton
              testID={testIDs.CHANGE_DISPLAY_NAME_BUTTON}
              style={{marginTop: 10}}
              onPress={changeDisplayNameHandler}>
              Change Display Name
            </BusyButton>
            <BusyButton
              testID={testIDs.CHANGE_PASSWORD_BUTTON}
              style={{marginTop: 10}}
              onPress={changePasswordHandler}>
              Change Password
            </BusyButton>
            <BusyButton style={{marginTop: 10}} onPress={bluetoothHandler}>
              Test Bluetooth
            </BusyButton>
          </Card.Actions>
        </Card>

        <Settings />
        <VideoConferenceGate />
      </ScrollView>
    </>
  );
};

export default UserRegistered;
