import React from 'react';
import {Avatar, Card, Title, Paragraph, List, Text} from 'react-native-paper';
import styles from './styles';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation, RouteProp, useRoute} from '@react-navigation/native';
import {
  RootState,
  JoinWaitingRoomResponse,
  RootStackParamList,
} from '../../types';
import BridgeModule from '../../nativemodules/BridgeModule';
import * as signalrApi from '../../actions/signalrApi';
import {ThunkDispatch} from 'redux-thunk';
import {AnyAction} from 'redux';
import BusyButton from '@components/BusyButton';
import testIDs from '@e2e/testIDs';
import {ScrollView} from 'react-native-gesture-handler';
import DoctorListItem from './DoctorListItem';

const homeImg = require('../../../assets/images/home.png');

const LeftContent = (props: any) => <Avatar.Icon {...props} icon="history" />;

const onlineDoctors = [
  'demo!',
  'Dr Siah Tan',
  'Dr Wan Kum Chan',
  'Dr Maja Markovic',
  'Dr Sue Phon',
  'Dr Barry Tan',
  'Dr Michael D’Ambrosio',
  'Dr Kalyanie Kuruppuarachchi',
];

const avatars = [
  require('../../../assets/images/doctors/demo.jpg'),
  require('../../../assets/images/doctors/0.jpg'),
  require('../../../assets/images/doctors/1.jpg'),
  require('../../../assets/images/doctors/2.jpg'),
  require('../../../assets/images/doctors/3.jpg'),
  require('../../../assets/images/doctors/4.jpg'),
  require('../../../assets/images/doctors/5.jpg'),
  require('../../../assets/images/doctors/6.jpg'),
];

const UserRegistered = () => {
  const dispatch = useDispatch<ThunkDispatch<RootState, void, AnyAction>>();
  const navigation = useNavigation();
  const route = useRoute<RouteProp<RootStackParamList, 'JoinWaitingRoom'>>();
  const displayName = useSelector((state: RootState) => state.displayName);
  const medicareCardNumberRaw = route.params?.medicareCardNumberRaw; //chain operator: will be undefined if no params

  const joinWaitingRoomHandler = (doctorName: string) => {
    let patientName = medicareCardNumberRaw ? null : displayName;

    dispatch(
      signalrApi.joinWaitingRoom(
        patientName,
        doctorName,
        medicareCardNumberRaw,
      ),
    ).then((result: JoinWaitingRoomResponse) => {
      if (result.isSuccess) {
        let ticketNumber = result.action.patient.ticketNumber;
        let qPos = result.queuePosition;
        BridgeModule.playJoinWaitingSound();
        navigation.reset({
          index: 0,
          routes: [
            {
              name: 'WaitingRoom',
              params: {queuePosition: qPos, ticketNumber: ticketNumber},
            },
          ],
        });
      }
    });
  };

  return (
    <>
      <ScrollView testID={testIDs.JOIN_WAITING_SCROLLVIEW}>
        <Card style={styles.card}>
          <Card.Title
            title={<Title>Join Waiting Room</Title>}
            subtitle=""
            left={LeftContent}
          />
          <Card.Content>
            <Paragraph>
              If you would like to request the next available doctor, please
              select the button below.
            </Paragraph>
            <BusyButton
              testID={testIDs.NEXT_AVAILABLE_BUTTON}
              onPress={() => joinWaitingRoomHandler(null)}>
              Request next available doctor
            </BusyButton>

            <Paragraph style={{marginTop: 20}}>
              Alternatively you can request a specific doctor.
            </Paragraph>

            <List.Section>
              <List.Accordion expanded={true} title="Doctors Online">
                {onlineDoctors.map((item, index) => {
                  return (
                    <DoctorListItem
                      index={index}
                      avatar={avatars[index]}
                      key={item}
                      name={item}
                      onPress={joinWaitingRoomHandler}
                    />
                  );
                })}
              </List.Accordion>
            </List.Section>
          </Card.Content>
        </Card>
      </ScrollView>
    </>
  );
};

export default UserRegistered;
