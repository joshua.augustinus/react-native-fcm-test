import React, {useState} from 'react';
import {ActivityIndicator, ImageSourcePropType, Image} from 'react-native';
import {List} from 'react-native-paper';
import {useSelector} from 'react-redux';
import {RootState} from '@src/types';
import styles from './styles';
import * as Animatable from 'react-native-animatable';

interface Props {
  name: string;
  onPress: (doctorName: string) => void;
  avatar: ImageSourcePropType;
  index: number;
}

const DoctorListItem = (props: Props) => {
  const [right, setRight] = useState(null);
  const isBusy = useSelector((state: RootState) => state.isBusy);
  const duration = props.index * 200 + 2000;
  return (
    <Animatable.View animation="bounceInLeft" duration={duration}>
      <List.Item
        disabled={isBusy}
        onPress={() => {
          setRight(<ActivityIndicator />);
          props.onPress(props.name);
        }}
        right={() => right}
        left={(x) => <Image source={props.avatar} style={styles.avatar} />}
        title={props.name}
      />
    </Animatable.View>
  );
};

export default DoctorListItem;
