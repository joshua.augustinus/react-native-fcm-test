import React, {useState} from 'react';
import {Alert} from 'react-native';
import {
  Avatar,
  Button,
  Card,
  Paragraph,
  useTheme,
  TextInput,
} from 'react-native-paper';
import styles from './styles';
import * as userApi from '../../actions/userApi';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {ThunkDispatch} from 'redux-thunk';
import {AnyAction} from 'redux';
import {useTypedSelector, RootState} from '@src/types';

const LeftContent = (props: any) => <Avatar.Icon {...props} icon="lock" />;

const ChangePassword = () => {
  const navigation = useNavigation();
  const [password, setPassword] = useState('');
  const [warning, setWarning] = useState('');
  const dispatch = useDispatch<ThunkDispatch<RootState, void, AnyAction>>();
  const isBusy = useTypedSelector((state) => state.isBusy);
  const username = useTypedSelector((state) => state.username);

  const submitHandler = () => {
    //verify username is available
    //update state to show loading

    if (password.length === 0) {
      setWarning('Password cannot be blank');
      return;
    }

    dispatch(userApi.changePassword(username, password)).then((result) => {
      if (result.error || result.status != 200) {
        Alert.alert('Failed to update password');
      } else {
        //Successful
        navigation.navigate('UserRegistered', {username: username});
      }
    });
  };

  return (
    <>
      <Card style={styles.card}>
        <Card.Title title="Change Password" subtitle="" left={LeftContent} />
        <Card.Content>
          <Paragraph>Please enter your new password.</Paragraph>

          <TextInput
            autoCapitalize="none"
            dense
            style={styles.inputContainerStyle}
            label="Password"
            placeholder="Cannot be blank"
            value={password}
            onChangeText={(text) => setPassword(text)}
          />
          <Paragraph style={{color: 'red'}}>{warning}</Paragraph>
        </Card.Content>
        <Card.Actions style={styles.buttonContainer}>
          <Button disabled={isBusy} loading={isBusy} onPress={submitHandler}>
            Submit
          </Button>
        </Card.Actions>
      </Card>
    </>
  );
};

export default ChangePassword;
