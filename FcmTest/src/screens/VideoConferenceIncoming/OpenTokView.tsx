import styles from './styles';
import React from 'react';
import {View, Dimensions} from 'react-native';
import {OTSession, OTPublisher, OTSubscriber} from 'opentok-react-native';

import BluetoothGate from '@components/Bluetooth/BluetoothGate';
import MeasurementNotification from '@components/Bluetooth/MeasurementNotification';

interface OpenTokViewProps {
  orientation: string;
  apiKey: string;
  token: string;
  sessionId: string;
}

const OpenTokView = (props: OpenTokViewProps) => {
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const orientation = props.orientation;
  const sessionOptions = {
    androidOnTop: 'publisher',
    androidZOrder: 'mediaOverlay',
    useTextureViews: true,
  };

  let publisherWrapperStyle =
    orientation === 'portrait'
      ? styles.publisherWrapperPortrait
      : styles.publisherWrapperLandscape;

  return (
    <View style={{flex: 1}}>
      <OTSession
        style={{flex: 1}}
        apiKey={props.apiKey}
        sessionId={props.sessionId}
        token={props.token}
        options={sessionOptions}>
        <OTSubscriber
          style={{
            width: windowWidth,
            height: windowHeight,
          }}></OTSubscriber>
        <View style={publisherWrapperStyle}>
          <OTPublisher style={styles.publisher}></OTPublisher>
        </View>
      </OTSession>
      <BluetoothGate />
      <MeasurementNotification />
    </View>
  );
};

export default OpenTokView;
