import React, {useEffect, useRef} from 'react';
import config from '@src/actions/config';
import * as signalR from '@microsoft/signalr';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {RootState} from '@src/types';

interface EndCallEvent {
  ticketNumber: number;
}

interface Props {
  /**
   * The route to navigate when received endCall event
   */
  leaveRoute: string;
}

function createHubConnection() {
  const url = config.signalr + '/hub';

  return new signalR.HubConnectionBuilder()
    .withUrl(url)
    .configureLogging(signalR.LogLevel.Error)
    .build();
}

/**
 * A hidden component that handles signalr events
 */
const SignalrListener = (props: Props) => {
  const hubRef = useRef(createHubConnection());
  const navigation = useNavigation();
  const ticketNumber = useSelector((state: RootState) => state.ticketNumber);
  /**
   * Function to start hub and retry if failed
   */
  const startConnection = async () => {
    try {
      console.log('startConnection called in SignalrListener');
      let hubConnection = hubRef.current;
      if (hubConnection.state !== signalR.HubConnectionState.Disconnected)
        return;
      await hubConnection.start();
      // Once started, invokes the sendConnectionId in our ChatHub inside our ASP.NET Core application.
      if (hubConnection.connectionId) {
        console.log('Hub connected');
      }
    } catch (err) {
      console.log(err);
      //Retry
      setTimeout(() => startConnection(), 5000);
    }
  };

  /**
   * Register signalr listeners
   */
  useEffect(() => {
    let hubConnection = hubRef.current;

    hubConnection.on('EndCall', (payload: EndCallEvent) => {
      console.log('Received end call event', payload);
      if (payload.ticketNumber === ticketNumber)
        navigation.reset({
          index: 0,
          routes: [{name: props.leaveRoute}],
        });
    });

    return () => {
      let hubConnection = hubRef.current;
      hubConnection.off('EndCall');
    };
    //The event handler will update itself everytime queuePosition changes
    //because we reference it in an event handler. If we don't update it, it will have a stale
    //reference to queuePosition
  }, []);

  useEffect(() => {
    // Starts the SignalR connection
    startConnection();

    return () => {
      console.log('Unmount SignalrListener');
      let hubConnection = hubRef.current;
      hubConnection.stop();
    };
  }, []);

  return <></>;
};

export {SignalrListener};
