import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  largeIcon: {
    color: 'white',
    fontSize: 280,
    alignSelf: 'center',
  },
  overallContainer: {
    display: 'flex',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#303030',
    height: '100%',
    width: '100%',
  },
  centerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconButton: {
    backgroundColor: '#4DBD33',
    borderRadius: 250,
    marginBottom: 20,
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconButtonSmall: {
    backgroundColor: 'red',
    borderRadius: 250,
    height: 50,
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 5,
  },
  text: {
    color: 'white',
    fontSize: 30,
    textAlign: 'center',
  },
  textSmall: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
  publisher: {
    height: '98%',
    width: '98%',
  },
  publisherWrapperPortrait: {
    position: 'absolute',
    height: '27%',
    width: '28%',
    bottom: 5,
    left: 5,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  publisherWrapperLandscape: {
    position: 'absolute',
    height: '32%',
    width: '21%',
    bottom: 5,
    left: 5,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});
