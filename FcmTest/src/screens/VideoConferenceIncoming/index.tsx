import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
import React, {useState, useEffect, useRef} from 'react';
import BridgeModule from '../../nativemodules/BridgeModule';
import styles from './styles';
import OpenTokView from './OpenTokView';
import Svg, {Path} from 'react-native-svg';
import * as signalrApi from '../../actions/signalrApi';
import {useSelector, useDispatch} from 'react-redux';
import {RootState, RootStackParamList} from '../../types';
import {useNavigation, useRoute} from '@react-navigation/native';
import {RouteProp} from '@react-navigation/native';
import testIDs from '@e2e/testIDs';
import {appHeaderVisibilityUpdated} from '@src/actions/actionCreator';
import {SignalrListener} from './SignalrListener';
/**
 * View State enum
 */
const stateType = {
  CALLING: 0,
  ANSWERED: 1,
};

function getOrientation() {
  const dim = Dimensions.get('screen');
  let isPortrait = dim.height >= dim.width;
  return isPortrait ? 'portrait' : 'landscape';
}

const VideoConferenceIncoming = () => {
  const dispatch = useDispatch();
  const isBusyRef = useRef(false);

  const [orientation, setOrientation] = useState(getOrientation());
  const navigation = useNavigation();
  const route = useRoute<
    RouteProp<RootStackParamList, 'VideoConferenceIncoming'>
  >();
  const [viewState, setViewState] = useState(
    route.params.launchMethod === 'notification'
      ? stateType.ANSWERED
      : stateType.CALLING,
  );
  const ticketNumber = useSelector((state: RootState) => state.ticketNumber);
  const callIgnoredRef = useRef(true);

  console.log('LaunchMethod', route.params.launchMethod);

  const onAnswer = () => {
    callIgnoredRef.current = false;
    BridgeModule.stopCallSound();
    BridgeModule.saveTicketNumber(-1);
    setViewState(stateType.ANSWERED);
  };

  const onIgnore = () => {
    if (isBusyRef.current == false) {
      isBusyRef.current = true;
      signalrApi.ignoreCall(ticketNumber).then((result) => {
        let qPos = result.queuePosition;
        console.log('Q Pos', qPos);
        navigation.reset({
          index: 0,
          routes: [
            {
              name: 'WaitingRoom',
              params: {queuePosition: qPos, ticketNumber: ticketNumber},
            },
          ],
        });
      });
    }
  };

  const handleDimensionChange = () => {
    let orientation = getOrientation();
    console.log('Orientation', orientation);
    setOrientation(orientation);
  };

  useEffect(() => {
    dispatch(appHeaderVisibilityUpdated(false));
    //Add event listener
    Dimensions.addEventListener('change', handleDimensionChange);

    if (viewState !== stateType.ANSWERED) BridgeModule.playCallSound();
    //Cleanup
    return () => {
      BridgeModule.stopCallSound();
      dispatch(appHeaderVisibilityUpdated(true));
      if (!callIgnoredRef.current) BridgeModule.saveTicketNumber(0); //delete the ticket
      Dimensions.removeEventListener('change', handleDimensionChange);
    };
  }, []);

  //Style based on orientation
  let iconButtonStyle;
  if (orientation == 'portrait') {
    iconButtonStyle = {...styles.iconButton, width: '70%'};
  } else {
    iconButtonStyle = {...styles.iconButton, height: '70%'};
  }

  if (viewState === stateType.ANSWERED) {
    return (
      <>
        <OpenTokView {...route.params} orientation={orientation} />
        <SignalrListener leaveRoute="UserRegistered" />
      </>
    );
  } else {
    //For svg creation see
    //https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/issues/49
    return (
      <View style={styles.overallContainer}>
        <View style={{...styles.centerContainer, flex: 3}}>
          <TouchableOpacity
            onPress={onAnswer}
            style={iconButtonStyle}
            testID={testIDs.ACCEPT_CALL_BUTTON}>
            <Svg height="80%" viewBox="0 0 24 24" width="80%">
              <Path d="M0 0h24v24H0z" fill="none" />
              <Path
                fill="white"
                d="M20.01 15.38c-1.23 0-2.42-.2-3.53-.56a.977.977 0 00-1.01.24l-1.57 1.97c-2.83-1.35-5.48-3.9-6.89-6.83l1.95-1.66c.27-.28.35-.67.24-1.02-.37-1.11-.56-2.3-.56-3.53 0-.54-.45-.99-.99-.99H4.19C3.65 3 3 3.24 3 3.99 3 13.28 10.73 21 20.01 21c.71 0 .99-.63.99-1.18v-3.45c0-.54-.45-.99-.99-.99z"
              />
            </Svg>
          </TouchableOpacity>

          <Text style={styles.text}>Incoming call from demo doctor</Text>
        </View>

        <View style={{...styles.centerContainer, flex: 1}}>
          <TouchableOpacity
            onPress={onIgnore}
            style={styles.iconButtonSmall}
            testID={testIDs.DECLINE_CALL_BUTTON}>
            <Svg height={35} viewBox="0 0 24 24" width={35}>
              <Path d="M0 0h24v24H0z" fill="none" />
              <Path
                fill="white"
                d="M12 9c-1.6 0-3.15.25-4.6.72v3.1c0 .39-.23.74-.56.9-.98.49-1.87 1.12-2.66 1.85-.18.18-.43.28-.7.28-.28 0-.53-.11-.71-.29L.29 13.08a.956.956 0 01-.29-.7c0-.28.11-.53.29-.71C3.34 8.78 7.46 7 12 7s8.66 1.78 11.71 4.67c.18.18.29.43.29.71 0 .28-.11.53-.29.71l-2.48 2.48c-.18.18-.43.29-.71.29-.27 0-.52-.11-.7-.28a11.27 11.27 0 00-2.67-1.85.996.996 0 01-.56-.9v-3.1C15.15 9.25 13.6 9 12 9z"
              />
            </Svg>
          </TouchableOpacity>

          <Text style={styles.textSmall}>Decline call</Text>
        </View>
        <SignalrListener leaveRoute="WaitingRoom" />
      </View>
    );
  }
};

export default VideoConferenceIncoming;
