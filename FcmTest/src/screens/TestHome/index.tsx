import React, {useState, useEffect} from 'react';
import {View, Text, PixelRatio, ScrollView} from 'react-native';
import {useWindowDimensions} from 'react-native';
import styles from './styles';
import {RootState, SavedRootState} from '../../types';

import {TextInput, Card, Avatar, Button} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import BridgeModule from '../../nativemodules/BridgeModule';
import {useNavigation} from '@react-navigation/native';
import testIDs from '@e2e/testIDs';
import {createSession} from './testFcmApi';
import {FcmBridge} from '@src/nativemodules/FcmBridge';

const LeftContent = (props: any) => <Avatar.Icon {...props} icon="tools" />;

const TestHome = () => {
  const windowWidth = useWindowDimensions().width;
  const windowHeight = useWindowDimensions().height;
  const [qPosition, setQPosition] = useState('0');
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const ticketNumber = useSelector((state: RootState) => state.ticketNumber);
  const [deviceInfo, setDeviceInfo] = useState(
    PixelRatio.getPixelSizeForLayoutSize(windowWidth) +
      ' x ' +
      PixelRatio.getPixelSizeForLayoutSize(windowHeight),
  );
  const displayName = useSelector((state: RootState) => state.displayName);
  const [registrationToken, setRegistrationToken] = useState(null);

  useEffect(() => {
    BridgeModule.loadAllSharedPreferences().then((result: SavedRootState) => {
      console.log('saved state loaded', result);
      dispatch({type: 'SAVED_STATE_LOADED', data: result});

      SplashScreen.hide();
    });

    FcmBridge.getRegistrationToken().then((token: string) => {
      setRegistrationToken(token);
    });
  }, []);

  const homeHandler = () => {
    navigation.navigate('HiddenScreen');
  };

  const waitingRoomHandler = () => {
    console.log(
      'Navigate to waiting room with qPosition: ',
      parseInt(qPosition),
    );

    navigation.navigate('WaitingRoom', {queuePosition: parseInt(qPosition)});
  };

  const resetHandler = () => {
    BridgeModule.resetSharedPreference().then((result) => {
      dispatch({type: 'SAVED_STATE_LOADED', data: result});
    });
  };

  const createSessionHandler = () => {
    createSession(ticketNumber);
  };

  const clearNotificationsHandler = () => {
    FcmBridge.clearNotifications();
  };

  const showVcHandler = () => {
    navigation.navigate('VideoConferenceIncoming', {launchMethod: 'e2e'});
  };

  return (
    <ScrollView testID="SCROLLVIEW_TESTHOME">
      <Card style={styles.card} testID={testIDs.TEST_HOME_CARD}>
        <Card.Title title="E2E Testing Home" subtitle="" left={LeftContent} />
        <Card.Content>
          <Button
            style={styles.button}
            icon="home"
            mode="contained"
            testID={testIDs.TRUE_HOME_BUTTON}
            onPress={homeHandler}>
            Go to true home page
          </Button>

          <TextInput
            testID={testIDs.Q_POSITION_INPUT}
            autoCapitalize="none"
            dense
            style={styles.inputContainerStyle}
            label="Queue Position"
            placeholder="Queue Position"
            value={qPosition.toString()}
            onChangeText={(text) => setQPosition(text)}
          />

          <Button
            style={styles.button}
            icon="seat"
            mode="contained"
            testID={testIDs.E2E_WAITING_BUTTON}
            onPress={() => waitingRoomHandler()}>
            Go to waiting room
          </Button>

          <Button
            style={styles.button}
            mode="contained"
            icon="autorenew"
            testID={testIDs.E2E_RESET_STATE_BUTTON}
            onPress={resetHandler}>
            Reset State
          </Button>
          <View>
            <TextInput
              testID={testIDs.TICKET_NUMBER_FIELD}
              autoCapitalize="none"
              dense
              style={styles.inputContainerStyle}
              label="Ticket Number"
              value={ticketNumber.toString()}
            />
            {ticketNumber <= 0 && (
              <Text
                testID={testIDs.INVALID_TICKET_WARNING}
                style={{color: 'red'}}>
                Invalid ticket number
              </Text>
            )}
            <Button
              style={styles.button}
              mode="contained"
              icon="video"
              testID={testIDs.CREATE_SESSION_BUTTON}
              onPress={createSessionHandler}>
              Create Session
            </Button>
          </View>
          <TextInput
            testID={testIDs.DEVICE_INFO_TEXT}
            autoCapitalize="none"
            dense
            style={styles.inputContainerStyle}
            label="Dimensions in pixels"
            value={deviceInfo}
          />
          <TextInput
            testID={testIDs.DISPLAY_NAME}
            autoCapitalize="none"
            dense
            style={styles.inputContainerStyle}
            label="Display Name"
            value={displayName}
          />
          <Button
            style={styles.button}
            mode="contained"
            testID={testIDs.CLEAR_NOTIFICATIONS_BUTTON}
            onPress={clearNotificationsHandler}>
            Clear Notifications
          </Button>
          <Button
            style={styles.button}
            mode="contained"
            testID={testIDs.SHOW_VIDEO_CONFERENCE_BUTTON}
            onPress={() => showVcHandler()}>
            Show Video Screen
          </Button>
          <TextInput
            testID={testIDs.TOKEN_FIELD}
            autoCapitalize="none"
            dense
            style={styles.inputContainerStyle}
            label="Registration Token"
            value={registrationToken}
          />
        </Card.Content>
      </Card>
    </ScrollView>
  );
};

export default TestHome;
