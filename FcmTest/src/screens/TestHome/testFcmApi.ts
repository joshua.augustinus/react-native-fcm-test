import * as baseApi from '@src/actions/baseApi';
import config from '@src/actions/config';
import {FcmBridge} from '@src/nativemodules/FcmBridge';

/**
 * Just allow us to make a fake request
 */

const baseUrl = config.fcm;

function getAsync(route: string) {
  return baseApi.getAsync(route, baseUrl);
}

function putAsync(route: string, data: any) {
  return baseApi.putAsync(route, data, baseUrl);
}

function postAsync(route: string, data: any) {
  return baseApi.postAsync(route, data, baseUrl);
}

export function createSession(ticketNumber: number) {
  return FcmBridge.getRegistrationToken().then((token) => {
    return postAsync('session', {
      ticketNumber: ticketNumber,
      registrationToken: token,
    });
  });
}
