import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  scrollView: {
    flex: 1,
  },
  card: {
    margin: 10,
    paddingHorizontal: 20,
  },
  imgCover: {
    borderRadius: 10,
    width: '100%',
    resizeMode: 'cover',
  },
  imgCoverContainer: {
    position: 'relative',

    marginTop: 5,
    borderRadius: 10,
    elevation: 10,
  },
  topCardContent: {
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  innerCard: {
    marginVertical: 5,
  },
  buttonContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  inputContainerStyle: {
    marginVertical: 8,
  },
  optionsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
  },
});

export default styles;
