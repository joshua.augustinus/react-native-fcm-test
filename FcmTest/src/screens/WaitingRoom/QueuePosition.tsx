import React from 'react';

import {Text, ActivityIndicator} from 'react-native-paper';

type Props = {
  queuePosition: number;
};

/**
 * The Waiting Room component
 */
const QueuePosition = (props: Props) => {
  console.log('QueuePosition props: ', props.queuePosition);
  if (props.queuePosition >= 0)
    return <Text style={{fontSize: 50}}>{props.queuePosition}</Text>;
  else return <ActivityIndicator style={{height: 100}} />;
};

export default QueuePosition;
