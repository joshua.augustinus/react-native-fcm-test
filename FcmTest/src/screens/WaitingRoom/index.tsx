import React, {useState, useEffect, useRef} from 'react';
import {Card, Paragraph, Subheading} from 'react-native-paper';
import styles from './styles';
import {useNavigation, RouteProp} from '@react-navigation/native';
import {RootState, WaitingSignalrEvent, RootStackParamList} from '../../types';
import {View, Image} from 'react-native';
import config from '../../actions/config';
import * as signalR from '@microsoft/signalr';
import BridgeModule from '../../nativemodules/BridgeModule';
import * as Animatable from 'react-native-animatable';
import {Dimensions} from 'react-native';
import testIDs from '@e2e/testIDs';
import BusyButton from '@components/BusyButton';
import {useSelector, useDispatch} from 'react-redux';
import * as signalrApi from '@src/actions/signalrApi';
import {ThunkDispatch} from 'redux-thunk';
import {AnyAction} from 'redux';
import QueuePosition from './QueuePosition';

const url = config.signalr + '/hub';
const windowWidth = Dimensions.get('window').width;
//https://stackoverflow.com/questions/44991669/react-native-require-with-dynamic-string
//Require cannot use dynamic strings
const imgSrc0 = require('../../../assets/images/waiting/0.jpg');
const imgSrc1 = require('../../../assets/images/waiting/1.jpg');
const imgSrc2 = require('../../../assets/images/waiting/2.jpg');
const imgSrc3 = require('../../../assets/images/waiting/3.jpg');
const imgSrc4 = require('../../../assets/images/waiting/4.jpg');
const imgSrc5 = require('../../../assets/images/waiting/5.jpg');
const imgSrc6 = require('../../../assets/images/waiting/6.jpg');
const imgSrc7 = require('../../../assets/images/waiting/7.jpg');
const imgSrc8 = require('../../../assets/images/waiting/8.jpg');
const imgSrc9 = require('../../../assets/images/waiting/9.jpg');
const imgSrc10 = require('../../../assets/images/waiting/10.jpg');

const pictures: any[] = [
  imgSrc0,
  imgSrc1,
  imgSrc2,
  imgSrc3,
  imgSrc4,
  imgSrc5,
  imgSrc6,
  imgSrc7,
  imgSrc8,
  imgSrc9,
  imgSrc10,
];

const ANIMATION_DURATION = 4000;

function getQueuePosition(evnt: WaitingSignalrEvent, ticketNumber: number) {
  console.log(evnt.waitingRoom.patients);
  for (let i = 0; i < evnt.waitingRoom.patients.length; i++) {
    let patient = evnt.waitingRoom.patients[i];

    if (patient.ticketNumber == ticketNumber) {
      console.log('Found index', i);
      return i + 1;
    }
  }

  return -1;
}

function getPictureSource(qPosition: number) {
  //we want an index between 0 and pictures.length - 1
  let index = qPosition % pictures.length;
  return pictures[index];
}

function createHubConnection() {
  return new signalR.HubConnectionBuilder()
    .withUrl(url)
    .configureLogging(signalR.LogLevel.Error)
    .build();
}

type RouteProps = RouteProp<RootStackParamList, 'WaitingRoom'>;

type Props = {
  route: RouteProps;
};

/**
 * The Waiting Room component
 */
const WaitingRoomScreen = (props: Props) => {
  const hubRef = useRef(createHubConnection());
  const navigation = useNavigation();
  const pictureRef = useRef(null);
  const ticketNumber = useSelector((state: RootState) => state.ticketNumber);
  const dispatch = useDispatch<ThunkDispatch<RootState, void, AnyAction>>();
  const qPosition = props.route.params ? props.route.params.queuePosition : -1;
  console.log('QPosition from route params', qPosition);
  const [queuePosition, setQueuePosition] = useState(qPosition);

  /**
   * Function to start hub and retry if failed
   */
  const startConnection = async () => {
    try {
      console.log('startConnection called in WaitingRoom');
      let hubConnection = hubRef.current;
      if (hubConnection.state !== signalR.HubConnectionState.Disconnected)
        return;
      await hubConnection.start();
      // Once started, invokes the sendConnectionId in our ChatHub inside our ASP.NET Core application.
      if (hubConnection.connectionId) {
        console.log('Hub connected');
      }
    } catch (err) {
      console.log(err);
      //Retry
      setTimeout(() => startConnection(), 5000);
    }
  };

  const leaveHandler = () => {
    let hubConnection = hubRef.current;
    hubConnection.off('Waiting');
    dispatch(signalrApi.leaveWaitingRoom(ticketNumber));
    navigation.reset({
      index: 0,
      routes: [{name: 'UserRegistered'}],
    });
  };

  useEffect(() => {
    let hubConnection = hubRef.current;

    hubConnection.on('Waiting', (payload: WaitingSignalrEvent) => {
      console.log('Received waiting event');
      let qPos = getQueuePosition(payload, ticketNumber);
      if (qPos < 0) qPos = 0;
      console.log('qPosition', queuePosition);
      if (qPos !== queuePosition) {
        BridgeModule.playNotificationSound();

        //addjust myState object without mutating so we can see i later

        setQueuePosition(qPos);
        pictureRef.current.fadeIn(ANIMATION_DURATION);
      }
    });

    return () => {
      let hubConnection = hubRef.current;
      hubConnection.off('Waiting');
    };
    //The event handler will update itself everytime queuePosition changes
    //because we reference it in an event handler. If we don't update it, it will have a stale
    //reference to queuePosition
  }, [queuePosition]);

  useEffect(() => {
    if (queuePosition < 0) {
      console.log('Fetching q position from api');
      signalrApi.getQueuePosition(ticketNumber).then((qPos) => {
        if (qPos > 0) {
          setQueuePosition(qPos);
        } else {
          navigation.reset({
            index: 0,
            routes: [{name: 'UserRegistered'}],
          });

          BridgeModule.saveTicketNumber(-1);
        }
      });
    }

    return () => {};
  }, []);

  useEffect(() => {
    // Starts the SignalR connection
    startConnection();

    return () => {
      console.log('Unmount WaitingRoom');
      let hubConnection = hubRef.current;
      hubConnection.stop();
    };
  }, []);

  const imgHeight = windowWidth / 2.3;

  return (
    <>
      <Card style={styles.card}>
        <Animatable.View
          style={styles.imgCoverContainer}
          testID={testIDs.WAITING_ROOM_VIEW}
          animation="fadeIn"
          duration={ANIMATION_DURATION}
          ref={pictureRef}>
          <Image
            source={getPictureSource(queuePosition)}
            style={{height: imgHeight, ...styles.imgCover}}
          />
        </Animatable.View>

        <Card.Title title="You have joined the Waiting Room" />
        <Card.Content style={styles.topCardContent}>
          <Paragraph>A doctor will be with you shortly.</Paragraph>
        </Card.Content>
      </Card>
      <Card style={styles.card}>
        <Card.Content>
          <View style={{alignItems: 'center'}}>
            <Subheading>Your position in the queue</Subheading>
            <QueuePosition queuePosition={queuePosition}></QueuePosition>
          </View>
        </Card.Content>
      </Card>
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
        }}>
        <BusyButton
          testID={testIDs.CANCEL_APPOINTMENT_BUTTON}
          onPress={leaveHandler}>
          Cancel Appointment
        </BusyButton>
      </View>
    </>
  );
};

export default WaitingRoomScreen;
