/**
 * Responsible for figuring out what screen to go to on load
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {useDispatch} from 'react-redux';
import BridgeModule from '../../nativemodules/BridgeModule';
import {SavedRootState, RootStackParamList} from '../../types';
import {useNavigation, useRoute, RouteProp} from '@react-navigation/native';
import * as signalrApi from '@src/actions/signalrApi';

const HiddenScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute<RouteProp<RootStackParamList, 'HiddenScreen'>>();

  const hideSplashScreen = () => {
    SplashScreen.hide();
  };

  useEffect(() => {
    /**
     * If we have the apiKey it is a VC call and we can navigate away immediately
     */
    if (route.params.apiKey) {
      navigation.reset({
        index: 0,
        routes: [{name: 'VideoConferenceIncoming', params: {...route.params}}],
      });
      hideSplashScreen();
    } else if (
      route.params.ticketNumber &&
      route.params.launchMethod === 'declineCall'
    ) {
      console.log('launchMethod: declineCall');
      console.log('ticketNumber', route.params.ticketNumber);
      const ticketNumber = parseInt(route.params.ticketNumber);

      signalrApi.ignoreCall(ticketNumber);
    }

    /**
     * Otherwise load our shared preferences to populate the store
     */

    BridgeModule.loadAllSharedPreferences().then((result: SavedRootState) => {
      console.log('saved state loaded', result);
      dispatch({type: 'SAVED_STATE_LOADED', data: result});
      let isIdle = true;
      const hasTicket = result.ticketNumber > 0 && result.access_token;
      console.log('Has ticket', hasTicket);
      //Now pick the route
      if (route.params.apiKey) {
        //Don't do anything we already navigated
      } else if (hasTicket) {
        navigation.reset({
          index: 0,
          routes: [{name: 'WaitingRoom'}],
        });
        isIdle = false; //When we navigate here we still want to fetch data
      } else if (result.username) {
        navigation.reset({
          index: 0,
          routes: [{name: 'UserRegistered'}],
        });
      } else {
        console.log('Going to createuser');
        navigation.reset({
          index: 0,
          routes: [{name: 'CreateUser'}],
        });
      }
      hideSplashScreen();
    });
  }, []);

  return <></>;
};

export default HiddenScreen;
