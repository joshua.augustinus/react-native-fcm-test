import React, {useState, useRef} from 'react';
import {
  Avatar,
  Card,
  TextInput,
  Caption,
  ProgressBar,
  Colors,
  Title,
} from 'react-native-paper';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import {RootStackParamList} from '@src/types';
import {StackNavigationProp} from '@react-navigation/stack';
import * as Animatable from 'react-native-animatable';
import testIDs from '@e2e/testIDs';
import {useWindowDimensions} from 'react-native';
import {Responsive} from '@components/Responsive';

const MEDICARE_TEXT_MAX = 27;
const medicareImg = require('../../../assets/images/medicare.jpg');

const LeftContent = (props: any) => <Avatar.Icon {...props} icon="card-text" />;

type NavigationProp = StackNavigationProp<RootStackParamList, 'MedicareSwipe'>;

const CardTitle = (
  <Responsive small="Swipe Medicare Card">
    <Title>Swipe the patient's Medicare card</Title>
  </Responsive>
);

const MedicareSwipe = () => {
  const textInput = useRef(null);
  const [warning, setWarning] = useState('');
  const navigation = useNavigation<NavigationProp>();
  const [medicare, setMedicare] = useState('');
  const [progress, setProgress] = useState(0);
  const windowWidth = useWindowDimensions().width;

  const finishSwipeHandler = () => {
    console.log('medicare', medicare);
    if (
      medicare.length > 0 &&
      medicare[medicare.length - 1] === '?' &&
      medicare.substring(0, 8).includes(';610072')
    )
      navigation.replace('JoinWaitingRoom', {
        medicareCardNumberRaw: medicare,
      });
    else {
      setWarning('Invalid Card. Swipe again.');
      setMedicare('');
      textInput.current.focus();
    }
  };

  const changeMedicareText = (text: string) => {
    setMedicare(text);
    setProgress(text.length / MEDICARE_TEXT_MAX);
  };

  const smallCardStyle = {
    resizeMode: 'contain',
    width: windowWidth * 0.8,
    alignSelf: 'center',
  };

  return (
    <>
      <Card style={styles.card} testID={testIDs.MEDICARE_CARD}>
        <Responsive smallStyle={smallCardStyle}>
          <Animatable.Image
            source={medicareImg}
            animation="bounceInRight"
            delay={500}
            duration={1000}
          />
        </Responsive>

        <Card.Title title={CardTitle} subtitle="" left={LeftContent} />
        <Card.Content>
          <ProgressBar
            progress={progress}
            color={Colors.green500}
            style={styles.progressBar}
          />
          <Caption style={{color: 'red'}}>{warning}</Caption>
        </Card.Content>
      </Card>
      <TextInput
        ref={textInput}
        showSoftInputOnFocus={false}
        value={medicare}
        onChangeText={changeMedicareText}
        onBlur={finishSwipeHandler}
        clearTextOnFocus={true}
        autoFocus={true}
        autoCapitalize="none"
        dense
        style={styles.textInput}
        label=""
        placeholder=""
        theme={{colors: {text: 'white'}}}
      />
    </>
  );
};

export default MedicareSwipe;
