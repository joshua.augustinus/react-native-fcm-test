import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  card: {
    margin: 10,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerCard: {
    marginVertical: 5,
  },

  inputContainerStyle: {
    marginVertical: 8,
  },
  progressBar: {},
  textInput: {
    opacity: 0.1,
    position: 'absolute',
    bottom: 0,
    fontSize: 5,
    backgroundColor: 'transparent',
  },
});

export default styles;
