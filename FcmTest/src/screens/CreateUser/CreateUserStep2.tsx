import React, {useState} from 'react';
import * as userApi from '../../actions/userApi';
import {useDispatch} from 'react-redux';
import {RootState} from '../../types';
import {useNavigation} from '@react-navigation/native';
import {ThunkDispatch} from 'redux-thunk';
import {AnyAction} from 'redux';
import UserPassForm from './UserPassForm';
import testIDs from '@e2e/testIDs';

const CreateUserStep2 = () => {
  const navigation = useNavigation();
  const [warning, setWarning] = useState('');
  const dispatch = useDispatch<ThunkDispatch<RootState, void, AnyAction>>();

  const submitHandler = (username: string, password: string) => {
    //verify username is available
    //update state to show loading

    if (username.length === 0) {
      setWarning('Username cannot be blank');
      return;
    }

    if (password.length === 0) {
      setWarning('Password cannot be blank');
      return;
    }

    dispatch(userApi.createUser(username, password)).then((result) => {
      if (result.message) {
        setWarning(result.message);
      } else {
        //User creation successful

        navigation.navigate('ChangeDisplayName');
      }
    });
  };

  return (
    <UserPassForm
      icon="account-plus"
      testID={testIDs.CREATE_USER_FORM}
      submitHandler={submitHandler}
      paragraph="Create new login"
      warning={warning}
      title="Create User"
    />
  );
};

export default CreateUserStep2;
