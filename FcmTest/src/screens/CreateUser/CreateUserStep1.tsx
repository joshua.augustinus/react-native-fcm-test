import * as React from 'react';
import {View} from 'react-native';
import {Button, Card, Paragraph} from 'react-native-paper';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import UserPassForm from './UserPassForm';
import * as userApi from '../../actions/userApi';
import {useDispatch} from 'react-redux';
import {ThunkDispatch} from 'redux-thunk';
import {RootState, HttpResponse} from '../../types';
import {AnyAction} from 'redux';
import {useState} from 'react';
import testIDs from '@e2e/testIDs';

const CreateUserStep1 = () => {
  const [warning, setWarning] = useState('');
  const navigation = useNavigation();
  const dispatch = useDispatch<ThunkDispatch<RootState, void, AnyAction>>();
  const createAccountHandler = () => {
    console.log('pressed');
    navigation.navigate('Step2');
  };

  const loginHander = (username: string, password: string) => {
    dispatch(userApi.getToken(username, password)).then(
      (result: HttpResponse) => {
        if (!result.isSuccess) {
          setWarning('Invalid login');
        } else {
          navigation.navigate('UserRegistered', {username: username});
          setWarning(''); //If the user presses back we don't want them to see warning; the state is saved even after we navigate
        }
      },
    );
  };

  return (
    <View testID={testIDs.LOGIN_TITLE}>
      <UserPassForm
        submitHandler={loginHander}
        paragraph="Enter your credentials"
        warning={warning}
        icon="account"
        title="Login"></UserPassForm>

      <Card style={styles.card}>
        <Card.Actions style={styles.optionsRow}>
          <Paragraph testID={testIDs.LOGIN_FOOTER}>
            Don't have an account?
          </Paragraph>
          <Button
            testID={testIDs.CREATE_ACCOUNT_BUTTON}
            onPress={createAccountHandler}>
            Create account
          </Button>
        </Card.Actions>
      </Card>
    </View>
  );
};

export default CreateUserStep1;
