import React, {useState} from 'react';
import {Avatar, Button, Card, Paragraph, TextInput} from 'react-native-paper';
import styles from './styles';
import {useSelector} from 'react-redux';
import {RootState} from '../../types';
import testIDs from '@e2e/testIDs';

interface Props {
  submitHandler: (username: string, password: string) => void;
  paragraph: string;
  warning: string;
  title: string;
  testID?: string;
  icon: string;
}

const CreateUserStep2 = (props: Props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVisibility, setPasswordVisibility] = useState(false);
  const isBusy = useSelector((state: RootState) => state.isBusy);

  let passwordIcon = passwordVisibility ? 'eye-off' : 'eye';

  const LeftContent = (contentProps: any) => (
    <Avatar.Icon {...contentProps} icon={props.icon} />
  );

  return (
    <Card testID={props.testID} style={styles.card}>
      <Card.Title title={props.title} subtitle="" left={LeftContent} />
      <Card.Content>
        <Paragraph>{props.paragraph}</Paragraph>

        <TextInput
          testID={testIDs.USERNAME_INPUT}
          autoCapitalize="none"
          dense
          style={styles.inputContainerStyle}
          label="Username"
          placeholder="Cannot be blank"
          value={username}
          onChangeText={(text) => setUsername(text)}
        />
        <TextInput
          testID={testIDs.PASSWORD_INPUT}
          secureTextEntry={!passwordVisibility}
          autoCapitalize="none"
          dense
          style={styles.inputContainerStyle}
          label="Password"
          placeholder="Cannot be blank"
          value={password}
          onChangeText={(text) => setPassword(text)}
          right={
            <TextInput.Icon
              name={passwordIcon}
              onPress={() => {
                setPasswordVisibility(!passwordVisibility);
              }}
            />
          }
        />
        <Paragraph style={{color: 'red'}}>{props.warning}</Paragraph>
      </Card.Content>
      <Card.Actions style={styles.buttonContainer}>
        <Button
          testID={testIDs.LOGIN_BUTTON}
          disabled={isBusy}
          loading={isBusy}
          onPress={() => props.submitHandler(username, password)}>
          Next
        </Button>
      </Card.Actions>
    </Card>
  );
};

export default CreateUserStep2;
