import React, {useState} from 'react';
import * as userApi from '../../actions/userApi';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../../types';
import {useNavigation} from '@react-navigation/native';
import {ThunkDispatch} from 'redux-thunk';
import {AnyAction} from 'redux';
import styles from './styles';
import {Card, Paragraph, TextInput, Avatar, Button} from 'react-native-paper';
import {Alert} from 'react-native';
import testIDs from '@e2e/testIDs';

const LeftContent = (props: any) => (
  <Avatar.Icon {...props} icon="account-edit" />
);

const ChangeDisplayName = () => {
  const navigation = useNavigation();
  const [warning, setWarning] = useState('');
  const username = useSelector((state: RootState) => state.username);
  const savedDisplayName = useSelector((state: RootState) => state.displayName);
  const [displayName, setDisplayName] = useState(
    savedDisplayName ? savedDisplayName : username,
  );
  const isBusy = useSelector((state: RootState) => state.isBusy);
  const dispatch = useDispatch<ThunkDispatch<RootState, void, AnyAction>>();

  const submitHandler = () => {
    //verify username is available
    //update state to show loading

    if (displayName.length === 0) {
      setWarning('Display Name cannot be blank');
      return;
    }

    dispatch(userApi.setDisplayName(username, displayName)).then((result) => {
      if (result.isSuccess) navigation.navigate('UserRegistered');
      else {
        Alert.alert('Something went wrong');
        console.log('Error', result);
      }
    });
  };

  return (
    <Card style={styles.card}>
      <Card.Title title="Set Display Name" subtitle="" left={LeftContent} />
      <Card.Content>
        <Paragraph>Enter how you want your name displayed to others.</Paragraph>

        <TextInput
          testID={testIDs.DISPLAY_NAME_INPUT}
          autoCapitalize="none"
          dense
          style={styles.inputContainerStyle}
          label="Display Name"
          placeholder="Cannot be blank"
          value={displayName}
          onChangeText={(text) => setDisplayName(text)}
        />
        <Paragraph style={{color: 'red'}}>{warning}</Paragraph>
      </Card.Content>
      <Card.Actions style={styles.buttonContainer}>
        <Button
          testID={testIDs.SET_DISPLAY_NAME_BUTTON}
          onPress={submitHandler}
          disabled={isBusy}
          loading={isBusy}>
          Submit
        </Button>
      </Card.Actions>
    </Card>
  );
};

export default ChangeDisplayName;
