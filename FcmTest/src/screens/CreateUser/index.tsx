import React from 'react';
import {View} from 'react-native';
import {useTheme} from 'react-native-paper';

import {createStackNavigator} from '@react-navigation/stack';
import CreateUserStep1 from './CreateUserStep1';
import CreateUserStep2 from './CreateUserStep2';
import ChangeDisplayName from './ChangeDisplayName';

const Stack = createStackNavigator();

const CreateUser = () => {
  const {colors} = useTheme();

  return (
    <>
      <View style={{flex: 1}}>
        <Stack.Navigator
          initialRouteName="Step1"
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="Step1" component={CreateUserStep1} />
          <Stack.Screen name="Step2" component={CreateUserStep2} />

          <Stack.Screen
            name="ChangeDisplayName"
            component={ChangeDisplayName}
          />
        </Stack.Navigator>
      </View>
    </>
  );
};

export default CreateUser;
