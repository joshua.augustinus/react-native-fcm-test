import React from 'react';

import {Card, Paragraph} from 'react-native-paper';
import styles from './styles';

interface Props {
  username: string;
}

const HeaderCard = (props: Props) => {
  let text = 'This application is currently not registered to a user.';
  if (props.username) text = 'This application is registered.';

  return (
    <Card style={styles.card}>
      <Card.Content>
        <Paragraph>{text}</Paragraph>
      </Card.Content>
    </Card>
  );
};

export default HeaderCard;
