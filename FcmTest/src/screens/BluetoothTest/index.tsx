import React from 'react';

import {View} from 'react-native';

import BluetoothGate from '@components/Bluetooth/BluetoothGate';
import MeasurementNotification from '@components/Bluetooth/MeasurementNotification';

function BluetoothTest() {
  return (
    <View
      style={{backgroundColor: 'grey', flex: 1, justifyContent: 'flex-end'}}>
      <BluetoothGate />
      <MeasurementNotification />
    </View>
  );
}

export {BluetoothTest};
