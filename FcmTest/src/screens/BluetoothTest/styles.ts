import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  card: {
    margin: 10,
    paddingHorizontal: 20,
  },
});

export default styles;
