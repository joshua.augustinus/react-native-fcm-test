import {PermissionsAndroid} from 'react-native';
import React from 'react';
import {PermissionGate} from '../PermissionGate';

const permissions = [
  PermissionsAndroid.PERMISSIONS.CAMERA,
  PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
  PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
];

/**
 * Just checks permissions and shows dialogs asking for permission
 * For this app we assume happy path i.e. our app will behave unexpectedly if the user
 * chooses to deny permissions.
 */
const VideoConferenceGate = () => {
  return <PermissionGate permissions={permissions} />;
};

export default VideoConferenceGate;
