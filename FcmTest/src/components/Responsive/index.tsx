import {useScreenInfo} from '@util/customHooks';
import React, {ReactNode, ReactElement} from 'react';

interface Props {
  /**
   * The element to show normally
   */
  children: ReactElement;
  /**
   * If smallStyle is defined, then the children will have this style applied
   * on small screens
   */
  smallStyle?: any;
  /**
   * If small is defined, then show this instead on small screens
   */
  small?: ReactNode;
}

const Responsive = (props: Props) => {
  const isSmallScreen = useScreenInfo().isSmallScreen;

  if (isSmallScreen) {
    if (props.smallStyle) {
      return React.cloneElement(props.children, {style: props.smallStyle});
    } else if (props.small) {
      return <>{props.small}</>;
    } else {
      return null;
    }
  } else {
    return <>{props.children}</>;
  }
};

export {Responsive};
