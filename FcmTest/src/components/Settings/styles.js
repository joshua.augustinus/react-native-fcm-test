import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  scrollView: {
    flex: 1,
  },
  card: {
    margin: 10,
    paddingHorizontal: 20,
  },
  innerCard: {
    marginVertical: 5,
  },
  buttonContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  inputContainerStyle: {
    marginVertical: 8,
  },
  optionsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
  },
  optionParagraph: {
    width: '85%',
    textAlign: 'right',
  },
});

export default styles;
