import React, {useState} from 'react';
import {View} from 'react-native';
import {
  Avatar,
  Card,
  Title,
  Paragraph,
  Checkbox,
  Caption,
} from 'react-native-paper';
import styles from './styles';
import {useSelector, useDispatch} from 'react-redux';
import BridgeModule from '../../nativemodules/BridgeModule';
import {useNavigation} from '@react-navigation/native';
import {RootState} from '../../types';
import BusyButton from '../BusyButton';
import testIDs from '@e2e/testIDs';

const LeftContent = (props: any) => <Avatar.Icon {...props} icon="wrench" />;

const DeveloperTools = () => {
  const navigation = useNavigation();
  const playSounds = useSelector(
    (state: RootState) => state.options.playSounds,
  );
  const isCareProvider = useSelector(
    (state: RootState) => state.options.isCareProvider,
  );
  console.log('isCareProvider', isCareProvider);

  const dispatch = useDispatch();

  const resetHandler = () => {
    //Delete Preferences
    BridgeModule.resetSharedPreference().then((result) => {
      //Redux dispatching is synchronous
      dispatch({type: 'SAVED_STATE_LOADED', data: result});

      console.log('Resetting route');
      navigation.reset({
        index: 0,
        routes: [{name: 'CreateUser', state: {routes: [{name: 'Step1'}]}}],
      });
    });
  };

  const playSoundsChecked = () => {
    dispatch({type: 'OPTIONS_UPDATED', data: {playSounds: !playSounds}});
    BridgeModule.saveAllSharedPreferences();
  };

  const isCareProviderChecked = () => {
    dispatch({
      type: 'OPTIONS_UPDATED',
      data: {isCareProvider: !isCareProvider},
    });
  };

  return (
    <Card style={styles.card}>
      <Card.Title
        title={<Title>Settings</Title>}
        subtitle=""
        left={LeftContent}
      />
      <Card.Content>
        <Card style={styles.innerCard}>
          <Card.Content>
            <Caption>State Management</Caption>
            <Paragraph>
              Resetting the application state removes all stored values.
            </Paragraph>
          </Card.Content>
          <Card.Actions style={styles.buttonContainer}>
            <BusyButton
              onPress={resetHandler}
              testID={testIDs.RESET_STATE_BUTTON}>
              Reset Application State
            </BusyButton>
          </Card.Actions>
        </Card>

        <Card style={styles.innerCard}>
          <Card.Content>
            <Caption>Options</Caption>

            <View style={styles.optionsRow}>
              <Paragraph style={styles.optionParagraph}>
                I'm a care provider and my patients have Medicare cards
              </Paragraph>
              <View testID={testIDs.IS_CARE_PROVIDER_CHECKBOX}>
                <Checkbox
                  status={isCareProvider ? 'checked' : 'unchecked'}
                  onPress={isCareProviderChecked}
                />
              </View>
            </View>
            <View style={styles.optionsRow}>
              <Paragraph style={styles.optionParagraph}>Play sounds</Paragraph>
              <View testID={testIDs.PLAY_SOUNDS_CHECKBOX}>
                <Checkbox
                  status={playSounds ? 'checked' : 'unchecked'}
                  onPress={playSoundsChecked}
                />
              </View>
            </View>
          </Card.Content>
        </Card>
      </Card.Content>
    </Card>
  );
};

export default DeveloperTools;
