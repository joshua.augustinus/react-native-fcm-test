import {Text, PermissionsAndroid, Permission} from 'react-native';
import React, {useState, useEffect} from 'react';

const permissionState = {
  GRANTED: 'GRANTED',
  DENIED: 'DENIED',
  UNKNOWN: 'UNKNOWN',
};

interface PermissionGateProps {
  permissions: Permission[];
}

async function asyncForEach(array: any[], callback: any) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index]);
  }
}

//Checks permissions
//If has correct permissions than returns BluetoothProvider
const PermissionGate = (props: PermissionGateProps) => {
  const [permissionCheck, setPermissionCheck] = useState(
    permissionState.UNKNOWN,
  );

  const allowedPermissions = [];

  const requestPermission = async () => {
    await asyncForEach(props.permissions, async (permission: Permission) => {
      try {
        const granted = await PermissionsAndroid.request(permission);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log(permission + ' granted');
          allowedPermissions.push(permission);
        } else {
          console.log(permission + ' denied');
        }
      } catch (err) {
        console.warn(err);
      }
    });

    if (allowedPermissions.length === props.permissions.length) {
      setPermissionCheck(permissionState.GRANTED);
    } else {
      setPermissionCheck(permissionState.DENIED);
    }
  };

  useEffect(() => {
    requestPermission();
  }, []);
  if (permissionCheck === permissionState.UNKNOWN)
    return <Text>Checking permissions</Text>;
  else if (permissionCheck === permissionState.DENIED)
    return <Text>One or more permissions were denied</Text>;
  else return <Text> </Text>; //All good
};

export {PermissionGate};
