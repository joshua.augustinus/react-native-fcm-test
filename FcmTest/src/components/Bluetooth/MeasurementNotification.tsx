/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useRef} from 'react';
import {View, Text} from 'react-native';
import styles from './styles';

import * as Animatable from 'react-native-animatable';
import {useTypedSelector, GenericMeasurement} from '../../types';
import BridgeModule from '../../nativemodules/BridgeModule';
import {BloodPressureRecord} from '@src/types/bluetooth';

const MeasurementNotification = () => {
  const measurement = useTypedSelector((state) => state.measurement);
  const componentRef = useRef<any>(null);
  useEffect(() => {
    if (componentRef && componentRef.current) {
      componentRef.current.flash(16000);
      BridgeModule.playNotificationSound();
    }
  }, [measurement]);

  if (measurement?.type === 'Blood Glucose') {
    const genericMeasurement = measurement as GenericMeasurement;
    return (
      <View style={styles.notification}>
        <Animatable.View ref={componentRef}>
          <Text style={styles.notificationText}>{measurement.type}</Text>

          <Text style={styles.measurementValueText}>
            {genericMeasurement.value.toFixed(1) +
              ' ' +
              genericMeasurement.unit}
          </Text>
        </Animatable.View>
      </View>
    );
  } else if (measurement?.type === 'Blood Pressure') {
    const bpMeasurement = measurement as BloodPressureRecord;
    return (
      <View style={styles.notification}>
        <Animatable.View ref={componentRef}>
          <Text style={styles.notificationText}>{measurement.type}</Text>

          <Text style={styles.measurementValueText}>
            {bpMeasurement.systolic.value +
              '/' +
              bpMeasurement.diastolic.value +
              ' ' +
              bpMeasurement.systolic.unit}
          </Text>
          <Text style={styles.measurementValueText}>
            {bpMeasurement.pulseRate.value + ' ' + bpMeasurement.pulseRate.unit}
          </Text>
        </Animatable.View>
      </View>
    );
  } else {
    return (
      <View style={styles.notification}>
        <Text style={styles.notificationText}>Awaiting Measurement</Text>
      </View>
    );
  }
};

export default MeasurementNotification;
