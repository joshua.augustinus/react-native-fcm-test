import {Base64} from 'js-base64';
import {Flag, BloodPressureRecord} from '@src/types/bluetooth';

const FLAGS = {
  IS_NOT_MMHG: {
    value: 0x01,
    name: 'If 0 then mmHg, If 1 then kPa',
  },
  TIME_STAMP_PRESENT: {
    value: 0x02,
    name: 'Time Stamp Flag',
  },
  PULSE_RATE_PRESENT: {value: 0x04, name: 'Pulse rate present'},
  USER_ID_PRESENT: {value: 0x08, name: 'User Id present'},
  MEASUREMENT_STATUS_PRESENT: {value: 0x10, name: 'Measurement status present'},
};

function base64ToArrayBuffer(base64: string) {
  var binary_string = Base64.atob(base64);
  var len = binary_string.length;
  var bytes = new Uint8Array(len);
  for (var i = 0; i < len; i++) {
    bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}

/**
 * For explanation:
 * https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/wikis/Bluetooth/Blood-Glucose-Standard
 */
export function parseBpMeasurement(base64Value: string): BloodPressureRecord {
  if (base64Value == null) throw 'base64 value cannot be null';

  let buffer = base64ToArrayBuffer(base64Value);
  let result = new DataView(buffer);
  let systolic: number;
  let diastolic: number;
  let pulseRate: number;

  let flags = result.getUint8(0);

  if (!hasFlag(FLAGS.IS_NOT_MMHG, flags)) {
    const systolicField = result.getUint16(1, true);
    systolic = getSFLOAT(systolicField);
    console.log('Systolic', systolic);

    const diastolicField = result.getUint16(3, true);
    diastolic = getSFLOAT(diastolicField);
    console.log('Diastolic', diastolic);

    const mapField = result.getUint16(5, true);
    const map = getSFLOAT(mapField);
    console.log('Mean arterial pressure', map);
  }

  let nextIndex = 7;
  if (hasFlag(FLAGS.TIME_STAMP_PRESENT, flags)) {
    //See here for date_time format:
    //https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/wikis/Bluetooth/Blood-Glucose-Standard
    nextIndex += 7;
  }
  if (hasFlag(FLAGS.PULSE_RATE_PRESENT, flags)) {
    const pulseField = result.getUint16(nextIndex, true);
    pulseRate = getSFLOAT(pulseField);
    nextIndex += 2;
    console.log('Pulse Rate', pulseRate);
  }

  const record: BloodPressureRecord = {
    type: 'Blood Pressure',
    systolic: {value: systolic, unit: 'mmHg'},
    diastolic: {value: diastolic, unit: 'mmHg'},
    pulseRate: {value: pulseRate, unit: 'bpm'},
  };

  return record;
}

function hasFlag(flag: Flag, v: number) {
  if (flag.value & v) {
    return true;
  }

  return false;
}

/** See
 *  https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/wikis/Bluetooth/SFloat-IEEE-Standard
 * */
function getSFLOAT(value: number) {
  console.log('inside getSFLOAT', value);
  switch (value) {
    case 0x07ff:
      return NaN;
    case 0x0800:
      return NaN;
    case 0x07fe:
      return Number.POSITIVE_INFINITY;
    case 0x0802:
      return Number.NEGATIVE_INFINITY;
    case 0x0801:
      return NaN;
    default:
      break;
  }

  let left4Bits = value >> 12;
  let right12Bits = value & 0x0fff;

  let exponent;
  let mantissa;

  //Check leftmost bit is 1
  if (left4Bits >= 0x0008) {
    //Two's complement
    //See https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/wikis/Bluetooth/Two's-complement
    exponent = -(0x0010 - left4Bits);
  } else {
    exponent = left4Bits;
  }

  if (right12Bits >= 0x0800) {
    mantissa = -(0x1000 - right12Bits);
  } else {
    mantissa = right12Bits;
  }

  return mantissa * 10 ** exponent;
}
