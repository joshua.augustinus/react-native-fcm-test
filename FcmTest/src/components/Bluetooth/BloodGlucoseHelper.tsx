import {Base64} from 'js-base64';
import {GlucoseRecord, Flag} from '../../types/bluetooth';
import * as dateTimeUtil from './DateTimeUtil';
const UNIT_TYPE = {
  MMOL_L: 'mmol/L',
  MG_DL: 'mg/dL',
};

const CONTEXT_FLAGS = {
  CARBS: {value: 0x01, name: 'Carbohydrate ID and Carbohydrate present'},
  MEAL: {value: 0x02, name: 'Meal present'},
  TESTER_HEALTH: {value: 0x04, name: 'Tester-Health present'},
  EXERCISE: {
    value: 0x08,
    name: 'Exercise Duration and Exercise Intensity present',
  },
  MEDICATION: {value: 0x10, name: 'Medication ID and Medication present'},
  UNITS: {value: 0x20, name: 'Medication value units'},
  HBA1C: {value: 0x40, name: 'HbA1c present'},
  EXTENDED: {value: 0x80, name: 'Extended flags present'},
};

const FLAGS = {
  TIME_OFFSET_PRESENT: {value: 0x01, name: 'Time offset present'},
  GLUCOSE_PRESENT: {
    value: 0x02,
    name: 'Glucose concentration, type and sample location present',
  },
  IS_MMOL: {value: 0x04, name: 'Glucose concentration units'},
  STATUS_PRESENT: {value: 0x08, name: 'Sensor status annunciation present'},
  CONTEXT_INFO: {value: 0x10, name: 'Context information follows'},
};

function base64ToArrayBuffer(base64: string) {
  var binary_string = Base64.atob(base64);
  var len = binary_string.length;
  var bytes = new Uint8Array(len);
  for (var i = 0; i < len; i++) {
    bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}

/**
 * For explanation:
 * https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/wikis/Bluetooth/Blood-Glucose-Standard
 */
export function parseGlucoseMeasurement(base64Value: string): GlucoseRecord {
  if (base64Value == null) throw 'base64 value cannot be null';

  let buffer = base64ToArrayBuffer(base64Value);
  let result = new DataView(buffer);
  console.log('parsing: ', buffer);

  let record = {
    ...new GlucoseRecord(),
    type: 'Blood Glucose',
    flags: result.getUint8(0),
    seqNum: result.getUint16(1, true),
  };
  console.log('record: ', record);
  let offset = 0;

  const dateTime = {
    year: result.getUint16(3, true),
    month: result.getUint8(5),
    day: result.getUint8(6),
    hours: result.getUint8(7),
    minutes: result.getUint8(8),
    seconds: result.getUint8(9),
  };

  if (hasFlag(FLAGS.TIME_OFFSET_PRESENT, record.flags)) {
    record.payload = {
      internalTime: dateTimeUtil.buildTimestamp(dateTime),
      timeOffset: result.getInt16(10, true),
    };
    record.timestamp = dateTimeUtil.applyOffset(
      record.payload.internalTime,
      record.payload.timeOffset,
    );
    offset += 2;
  } else {
    record.timestamp = dateTimeUtil.buildTimestamp(dateTime);
  }

  if (hasFlag(FLAGS.GLUCOSE_PRESENT, record.flags)) {
    if (hasFlag(FLAGS.IS_MMOL, record.flags)) {
      record.unit = UNIT_TYPE.MMOL_L;
    } else {
      record.unit = UNIT_TYPE.MG_DL;
    }
    console.log('offset: ', offset);
    record.value = getSFLOAT(result.getUint16(offset + 10, true), record.unit);

    //For possible type values see:
    //https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.glucose_measurement.xml
    record.bloodGlucoseType = result.getUint8(offset + 12) >> 4;
    record.location = result.getUint8(offset + 12) && 0x0f;

    if (hasFlag(FLAGS.STATUS_PRESENT, record.flags)) {
      record.status = result.getUint16(offset + 13, true);
    }
  } else {
    console.log(
      'No glucose value present for ',
      dateTimeUtil.formatDeviceTime(record.timestamp),
    );
  }

  record.hasContext = hasFlag(FLAGS.CONTEXT_INFO, record.flags);
  console.log('parsed value: ', record);
  return record;
}

/**
 * Converts to standard unit and returns an object
 * @param {GlucoseRecord} record
 * @returns {GlucoseRecord} object with units, value, type
 */
export function convertToStandardPayload(record: GlucoseRecord): GlucoseRecord {
  if (record.unit === UNIT_TYPE.MG_DL) {
    let result = {
      unit: UNIT_TYPE.MMOL_L,
      value: record.value / 18.0182,
      type: 'Blood Glucose',
    };
    return {...record, ...result};
  } else {
    return record;
  }
}

function hasFlag(flag: Flag, v: number) {
  if (flag.value & v) {
    return true;
  }

  return false;
}

/** See
 *  https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/wikis/Bluetooth/SFloat-IEEE-Standard
 * */
function getSFLOAT(value: number, units: string) {
  console.log('inside getSFLOAT', value);
  switch (value) {
    case 0x07ff:
      return NaN;
    case 0x0800:
      return NaN;
    case 0x07fe:
      return Number.POSITIVE_INFINITY;
    case 0x0802:
      return Number.NEGATIVE_INFINITY;
    case 0x0801:
      return NaN;
    default:
      break;
  }

  let left4Bits = value >> 12;
  let right12Bits = value & 0x0fff;

  let exponent;
  let mantissa;

  //Check leftmost bit is 1
  if (left4Bits >= 0x0008) {
    //Two's complement
    //See https://gitlab.com/joshua.augustinus/react-native-fcm-test/-/wikis/Bluetooth/Two's-complement
    exponent = -(0x0010 - left4Bits);
  } else {
    exponent = left4Bits;
  }

  if (units === UNIT_TYPE.MG_DL) {
    exponent += 5; // convert kg/L to mg/dL
  } else if (units === UNIT_TYPE.MMOL_L) {
    exponent += 3; // convert mol/L to mmol/L
  } else {
    throw Error('Illegal units for glucose value');
  }

  if (right12Bits >= 0x0800) {
    mantissa = -(0x1000 - right12Bits);
  } else {
    mantissa = right12Bits;
  }

  return mantissa * 10 ** exponent;
}
