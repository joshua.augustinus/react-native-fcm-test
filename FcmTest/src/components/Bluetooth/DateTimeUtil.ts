import moment from 'moment';

export interface BluetoothDateTime {
  year: number;
  month: number;
  day: number;
  hours: number;
  minutes: number;
  seconds: number;
}

export function applyOffset(timestamp: Date, offset: number) {
  if (!timestamp) {
    throw new Error('No timestamp provided as first argument!');
  }
  return moment.utc(timestamp).add(offset, 'minutes').toDate();
}

export function buildTimestamp(o: BluetoothDateTime): Date {
  let d = Date.UTC(o.year, o.month - 1, o.day, o.hours, o.minutes, o.seconds);
  if (isNaN(d)) {
    return null;
  }
  return new Date(d);
}

export function formatDeviceTime(dt: Date): string {
  if (!dt) {
    throw new Error('No datetime provided as first argument!');
  }
  // use of .utc() here is for cross-browser consistency
  return moment.utc(dt).format('YYYY-MM-DDTHH:mm:ss');
}
