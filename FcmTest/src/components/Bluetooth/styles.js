import {StyleSheet} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  bluetoothProviderText: {
    marginLeft: 5,
    color: Colors.white,
  },
  notification: {
    paddingHorizontal: 24,
    position: 'absolute',
    bottom: 5,
    right: 5,
    backgroundColor: '#303030',
    borderRadius: 10,
  },
  notificationText: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  measurementValueText: {
    fontSize: 18,
    fontWeight: '400',
    color: Colors.white,
    textAlign: 'right',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default styles;
