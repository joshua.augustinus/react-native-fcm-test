import BluetoothProvider from './BluetoothProvider';
import React from 'react';

/**
 * This component used to check permissions but that check now happens inside VideoConferenceGate
 */
const BluetoothGate = () => {
  return <BluetoothProvider />;
};

export default BluetoothGate;
