import React, {useState, useEffect, useRef} from 'react';
import {
  BleManager,
  BleError,
  Device,
  Characteristic,
} from 'react-native-ble-plx';
import * as BloodGlucoseHelper from './BloodGlucoseHelper';
import {View, Text, ToastAndroid} from 'react-native';
import {useDispatch} from 'react-redux';
import styles from './styles';
import * as signalrApi from '../../actions/signalrApi';
import {Switch} from 'react-native-paper';
import {parseBpMeasurement} from './BpHelper';
import {BluetoothHandler} from '@src/types/bluetooth';

const BLUETOOTH_STATE_SCANNING = 'Bluetooth Scanning';
const BLUETOOTH_STATE_PAUSED = 'Bluetooth Paused';
/**
 * https://www.bluetooth.com/specifications/gatt/services/
 */
const GLUCOSE_SERVICE = '00001808-0000-1000-8000-00805f9b34fb';
const GLUCOSE_CHARACTERISTIC = '00002a18-0000-1000-8000-00805f9b34fb';
const BP_SERVICE = '00001810-0000-1000-8000-00805f9b34fb';
const BP_CHARACTERISTIC = '00002A35-0000-1000-8000-00805f9b34fb';
const SERVICE_ARRAY = [GLUCOSE_SERVICE, BP_SERVICE];

function BluetoothProvider() {
  const [manager, setManager] = useState(new BleManager());
  const [bluetoothState, setBluetoothState] = useState(BLUETOOTH_STATE_PAUSED);

  const dispatch = useDispatch();
  const subscription = useRef<any>(null);
  const isSwitchOn = bluetoothState === BLUETOOTH_STATE_SCANNING;
  /**
   * Do stuff at the start e.g. scanning
   */
  useEffect(() => {
    // Scan devices only once
    if (bluetoothState == BLUETOOTH_STATE_SCANNING)
      manager.startDeviceScan(SERVICE_ARRAY, null, scanHandler);

    return () => {
      //Componount unmount code
      console.log('Unmounting BluetoothProvider');
      if (subscription.current) {
        console.log('unsub');
        subscription.current.remove();
      }

      manager.stopDeviceScan();
    };
  }, []);

  const onPressHandler = () => {
    console.log('Pressed');
    if (bluetoothState == BLUETOOTH_STATE_PAUSED) {
      manager.startDeviceScan(SERVICE_ARRAY, null, scanHandler);
      setBluetoothState(BLUETOOTH_STATE_SCANNING);
    } else {
      manager.stopDeviceScan();
      setBluetoothState(BLUETOOTH_STATE_PAUSED);
    }
  };

  /**
   * Connect bluetooth device
   */
  const connectDevice = (
    device: Device,
    service: string,
    characteristic: string,
    handler: BluetoothHandler,
  ) => {
    manager.stopDeviceScan();

    device
      .connect()
      .then((device) => {
        return device.discoverAllServicesAndCharacteristics();
      })
      .then((device) => {
        if (subscription.current) {
          console.log('remove subscription');
          subscription.current.remove();
        }

        subscription.current = device.monitorCharacteristicForService(
          service,
          characteristic,
          handler,
        );
      })

      .catch((error: BleError) => {
        // Handle errors

        console.log('device connect error', error);
        ToastAndroid.show(error.message, ToastAndroid.SHORT);
        manager.stopDeviceScan();
        manager.startDeviceScan(SERVICE_ARRAY, null, scanHandler);
      });
  };

  /**
   * Handle scanning
   */
  const scanHandler = (error: BleError | null, device: Device | null) => {
    if (error || !device) {
      //handle error
      console.log(JSON.stringify(error, null, 2));

      //Update state to show error
      return;
    } else {
      if (device.name !== null && device.name.substring(0, 5) === 'meter') {
        console.log('Found meter');

        connectDevice(
          device,
          GLUCOSE_SERVICE,
          GLUCOSE_CHARACTERISTIC,
          listenerHandler,
        );
      } else if (device.name.toUpperCase().substring(0, 8) === 'BLESMART') {
        console.log('device:', device.name);

        connectDevice(device, BP_SERVICE, BP_CHARACTERISTIC, bpListenHandler);
      } else {
        console.log('Found unknown device', device.name);
      }
    }
  };

  /**
   * Handle BP Characteristic monitoring
   */
  const bpListenHandler = (error: BleError, characteristic: Characteristic) => {
    if (error && error.reason) {
      console.log(error);
      ToastAndroid.show(error.reason, ToastAndroid.SHORT);
      //here i want to start scanning again
    } else if (characteristic != null) {
      console.log('characteristic value:', characteristic.value);
      const bpMeasurement = parseBpMeasurement(characteristic.value);
      dispatch({type: 'MEASUREMENT_UPDATED', data: bpMeasurement});
      signalrApi.notifyBloodPressure(bpMeasurement);
    }
    //device scan would have stopped when found device
    manager.startDeviceScan(SERVICE_ARRAY, null, scanHandler);
  };

  /**
   * Handle characteristic monitoring
   */
  const listenerHandler = (error: BleError, characteristic: Characteristic) => {
    console.log('inside listener');
    if (error && error.reason) {
      console.log(error);
      ToastAndroid.show(error.reason, ToastAndroid.SHORT);
      //here i want to start scanning again
    } else if (characteristic != null) {
      let result = BloodGlucoseHelper.parseGlucoseMeasurement(
        characteristic.value,
      );

      let standardResult = BloodGlucoseHelper.convertToStandardPayload(result);
      console.log('Result', result);
      dispatch({type: 'MEASUREMENT_UPDATED', data: standardResult});
      signalrApi.notifyMeasurement(standardResult);
    }

    manager.startDeviceScan(SERVICE_ARRAY, null, scanHandler);
  };

  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <Switch value={isSwitchOn} onValueChange={() => onPressHandler()} />
      <Text style={styles.bluetoothProviderText}>{bluetoothState}</Text>
    </View>
  );
}

export default BluetoothProvider;
