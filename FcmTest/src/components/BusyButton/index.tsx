import React, {useState} from 'react';

import {useDispatch, useSelector} from 'react-redux';
const shortid = require('shortid');

import {RootState, ButtonMode} from '../../types';
import {Button} from 'react-native-paper';
import {useScreenInfo} from '@util/customHooks';

interface Props {
  children: any;
  onPress: () => void;
  testID?: string;
  style?: any;
  mode?: ButtonMode;
  icon?: string;
}

const BusyButton = (props: Props) => {
  const isBusy = useSelector((state: RootState) => state.isBusy);
  const idClicked = useSelector((state: RootState) => state.idClicked);
  const dispatch = useDispatch();
  const [id, setId] = useState(shortid.generate());
  const isSmallScreen = useScreenInfo().isSmallScreen;
  //consume props
  const {onPress, ...rest} = props;

  const clickHandler = () => {
    dispatch({type: 'BUTTON_CLICKED', idClicked: id});
    if (onPress) onPress();
  };

  const labelStyle = isSmallScreen ? {fontSize: 12} : {};
  return (
    <Button
      labelStyle={labelStyle}
      icon={props.icon}
      mode={props.mode}
      style={{...props.style, fontSize: 5}}
      testID={props.testID}
      disabled={isBusy}
      loading={isBusy && idClicked == id}
      onPress={clickHandler}>
      {props.children}
    </Button>
  );
};

export default BusyButton;
