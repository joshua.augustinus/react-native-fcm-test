import React from 'react';
import {Text, Title, useTheme} from 'react-native-paper';
import * as Animatable from 'react-native-animatable';
import Config from 'react-native-config';
import * as RootNavigation from '@src/RootNavigation';
import testIDs from '@e2e/testIDs';
import styles from './styles';
import {useSelector} from 'react-redux';
import {RootState} from '@src/types';
import {View, TouchableOpacity} from 'react-native';
import {Subtitle} from './Subtitle';

const AppHeader = () => {
  const {colors} = useTheme();
  const isVisible = useSelector(
    (state: RootState) => state.appHeaderVisibility,
  );
  const displayName = useSelector((state: RootState) => state.displayName);
  const isTestMode = Config.ENVIRONMENT === 'TEST';
  let subTitle: React.ReactNode;
  if (displayName) subTitle = <Subtitle displayName={displayName} />;
  else subTitle = <Text>App is not registered</Text>;

  const appBarPressed = () => {
    if (isTestMode) RootNavigation.reset('TestHome');
  };

  return (
    isVisible && (
      <Animatable.View
        animation="fadeInDown"
        style={{...styles.container, backgroundColor: colors.primary}}>
        <Title testID={testIDs.HEADER_TITLE} style={styles.headerText}>
          Demo Project
        </Title>

        <View style={styles.subtitleContainer}>
          <Subtitle displayName={displayName} style={{flex: 1}} />
          <TouchableOpacity testID={testIDs.APP_HEADER} onPress={appBarPressed}>
            <Text style={styles.subtitleText}>
              Environment: {Config.ENVIRONMENT}
            </Text>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    )
  );
};

export default AppHeader;
