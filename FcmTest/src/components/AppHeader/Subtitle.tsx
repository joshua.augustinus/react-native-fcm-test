import React from 'react';
import {Text} from 'react-native-paper';

import styles from './styles';

import {View} from 'react-native';
import {Responsive} from '../Responsive';

interface SubtitleProps {
  displayName: string;
  style?: any;
}

/**
 * Subtitle on small screens
 */
const smallSubtitle = (displayName: string) => {
  return <Text style={styles.subtitleText}>{displayName}</Text>;
};

const Subtitle = (props: SubtitleProps) => {
  if (props.displayName) {
    return (
      <View style={props.style}>
        <Responsive small={smallSubtitle(props.displayName)}>
          <Text style={styles.subtitleText}>
            App is registered to: {props.displayName}
          </Text>
        </Responsive>
      </View>
    );
  } else {
    return <Text style={styles.subtitleText}>App is not registered</Text>;
  }
};

export {Subtitle};
