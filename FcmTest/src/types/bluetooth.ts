import {GenericMeasurement} from './index';
import {BleError, Characteristic} from 'react-native-ble-plx';

export class GlucoseRecord implements GenericMeasurement {
  constructor() {
    this.type = '';
    this.flags = NaN;
    this.seqNum = NaN;
    this.payload = {internalTime: null, timeOffset: null};
    this.unit = '';
    this.value = NaN;
    this.bloodGlucoseType = NaN;
    this.location = NaN;
    this.status = NaN;
    this.hasContext = false;
  }
  type: string;
  flags: number;
  seqNum: number;
  payload: RecordPayload;
  timestamp: Date;
  unit: string;
  value: number;
  /**See the bluetooth standard for possible values */
  bloodGlucoseType: number;
  location: number;
  status: number;
  hasContext: boolean;
}

export interface RecordPayload {
  internalTime: Date;
  timeOffset: any;
}

export interface Flag {
  value: number;
  name: string;
}

export interface BloodPressureRecord {
  type: string;
  systolic: MeasurementValue;
  diastolic: MeasurementValue;
  pulseRate: MeasurementValue;
}

export interface MeasurementValue {
  unit: string;
  value: number;
}

export type BluetoothHandler = (
  error: BleError,
  characteristic: Characteristic,
) => void;
