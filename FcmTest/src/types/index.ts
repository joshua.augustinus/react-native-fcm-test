import {TypedUseSelectorHook, useSelector} from 'react-redux';
import {BloodPressureRecord} from './bluetooth';

export interface RootState extends SavedRootState {
  isLoaded: boolean;
  username: string;
  displayName: string;
  isBusy: boolean;
  options: {
    playSounds: boolean;
    isCareProvider: boolean;
  };
  measurement: Measurement;
  access_token: string;
  idClicked: number;
  fcmProps?: FcmProps;
  ticketNumber: number;
  appHeaderVisibility: boolean;
}

export interface SavedRootState {
  username: string;

  options: {
    playSounds: boolean;
    isCareProvider: boolean;
  };

  access_token: string;
  ticketNumber: number;
  displayName: string;
}

/**
 * Define params for our routes: https://reactnavigation.org/docs/typescript/
 */
export type StackParamList = {
  Step1: undefined;
  Step2: undefined;
  UserRegistered: undefined;
};

export type RootStackParamList = {
  HiddenScreen: FcmProps;
  VideoConferenceIncoming: FcmProps;
  CreateUser: undefined;
  WaitingRoom: {ticketNumber: number; queuePosition: number};
  TestHome: undefined;
  JoinWaitingRoom: {medicareCardNumberRaw?: string};
  UserRegistered: undefined;
  ChangePassword: undefined;
  MedicareSwipe: undefined;
  BluetoothTest: undefined;
};

export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;

export type Measurement = BloodPressureRecord | GenericMeasurement;

export interface GenericMeasurement {
  type: string;
  unit: string;
  value: number;
}

export interface HttpResponse {
  isSuccess: boolean;
  status: number;
}

export interface Patient {
  name: string;
  medicareNumber: string;
  ticketNumber: number;
}

export interface WaitingRoom {
  patients: Patient[];
}

export interface WaitingSignalrEvent {
  waitingRoom: WaitingRoom;
}

export interface JoinWaitingRoomResponse extends HttpResponse {
  action: WaitingRoomAction;
  queuePosition: number;
}

export interface WaitingRoomAction {
  type: string;
  patient: Patient;
}

export interface FcmProps {
  apiKey: string;
  sessionId: string;
  token: string;
  launchMethod: string;
  ticketNumber?: string;
}

export type ButtonMode = 'text' | 'outlined' | 'contained';
