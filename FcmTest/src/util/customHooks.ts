import {useWindowDimensions} from 'react-native';
import {screenSize} from './screenSize';

/**
 * ALlows us to get screen dimensions but also tells if the screen is small
 */
const useScreenInfo = () => {
  const windowInfo = useWindowDimensions();
  return {
    ...windowInfo,
    isSmallScreen: windowInfo.width < screenSize.SMALL_WIDTH,
  };
};

export {useScreenInfo};
