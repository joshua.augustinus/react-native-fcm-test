//Due to the small nature of the project we are not doing action creators for everything

export function busyUpdated(isBusy: boolean) {
  return {
    type: 'BUSY_UPDATED',
    isBusy: isBusy,
  };
}

export function appHeaderVisibilityUpdated(visibility: boolean) {
  return {
    type: 'APPHEADER_VISIBILITY_UPDATED',
    appHeaderVisibility: visibility,
  };
}
