/* The FCM API was originally used only for Firebase Cloud Messaging (FCM) but now it's features 
include new user creation */
import * as baseApi from './baseApi';
import config from './config';
import {AnyAction} from 'redux';
import {Dispatch} from 'react';
import {busyUpdated} from './actionCreator';
import {HttpResponse} from '../types';

const baseUrl = config.user;

function getAsync(route: string) {
  return baseApi.getAsync(route, baseUrl);
}

function putAsync(route: string, data: any) {
  return baseApi.putAsync(route, data, baseUrl);
}

function postAsync(route: string, data: any) {
  return baseApi.postAsync(route, data, baseUrl);
}

export function createUser(username: string, password: string) {
  return function (dispatch: Dispatch<AnyAction>) {
    dispatch(busyUpdated(true));
    let payload = {username: username, password: password};
    return postAsync('user', payload)
      .then((result: any) => {
        console.log('Create user result', result);
        dispatch(busyUpdated(false));
        dispatch({type: 'ACCESSTOKEN_UPDATED', data: result.access_token});
        dispatch({type: 'USERNAME_UPDATED', data: username});
        return result;
      })
      .catch((error) => {
        console.log('Error', error);
        dispatch(busyUpdated(false));

        return error;
      });
  };
}

export function changePassword(username: string, password: string) {
  return (dispatch: Dispatch<AnyAction>) => {
    dispatch(busyUpdated(true));
    let payload = {password: password};
    return putAsync('user/' + username, payload)
      .then((result) => {
        console.log('Change password result', result);
        dispatch(busyUpdated(false));

        return result;
      })
      .catch((error) => {
        console.log('Error', error);
        dispatch(busyUpdated(false));

        return error;
      });
  };
}

interface GetTokenResponse extends HttpResponse {
  access_token: string;
  token_type: string;
  displayName: string;
}

export function getToken(username: string, password: string) {
  return function (dispatch: any) {
    dispatch(busyUpdated(true));
    let payload = {username: username, password: password};
    return postAsync('token', payload)
      .then((result: GetTokenResponse) => {
        dispatch(busyUpdated(false));
        dispatch({type: 'ACCESSTOKEN_UPDATED', data: result.access_token});
        dispatch({type: 'USERNAME_UPDATED', data: username});
        dispatch({
          type: 'DISPLAY_NAME_UPDATED',
          displayName: result.displayName,
        });
        return result;
      })
      .catch((error) => {
        dispatch(busyUpdated(false));
        dispatch({type: 'ACCESSTOKEN_UPDATED', data: null});
        return {...error, isSuccess: false};
      });
  };
}

export function setDisplayName(username: string, displayName: string) {
  let route = 'user/' + username;
  console.log('Route', route);
  return function (dispatch: any) {
    dispatch(busyUpdated(true));
    return putAsync(route, {displayName: displayName}).then((result) => {
      dispatch(busyUpdated(false));
      dispatch({type: 'DISPLAY_NAME_UPDATED', displayName: displayName});
      return result;
    });
  };
}
