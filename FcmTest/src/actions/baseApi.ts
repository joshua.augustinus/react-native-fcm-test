import store from '../store';
import {RootState} from '../types';

export function getAsync(route: string, baseUrl: string) {
  var url = baseUrl + '/' + route;
  var accessToken = getAccessToken();

  if (!accessToken) throw "Couldn't get access token";

  return fetch(url, {
    headers: {
      'content-type': 'application/json',
      Authorization: 'Bearer ' + accessToken,
    },
    mode: 'cors',
  })
    .then((response) => response.json())
    .catch((error) => {
      console.error(error);
      throw error;
    });
}

export function putAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, 'PUT', baseUrl);
}

export function postAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, 'POST', baseUrl);
}

export function deleteAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, 'DELETE', baseUrl);
}

function pAsync(route: string, data: any, method: string, baseUrl: string) {
  let url = baseUrl + '/' + route;
  let accessToken = getAccessToken();
  let status: number;
  let stringData = JSON.stringify(data);

  console.log(stringData);

  return fetch(url, {
    headers: {
      'content-type': 'application/json',
      Authorization: 'Bearer ' + accessToken,
    },
    method: method,
    body: stringData,
    mode: 'cors',
  })
    .then((response) => {
      status = response.status;

      //Status 204 will not have the content-length header
      let contentType = response.headers.get('Content-Type');
      if (contentType) {
        if (contentType.includes('application/json')) {
          console.log('Attempting response.json');
          return response.json();
        } else if (contentType.includes('text')) {
          return response.text();
        }
      } else return {};
    })
    .then((responseJson) => {
      if (typeof responseJson === 'string') {
        responseJson = {message: responseJson};
      }
      let isSuccess = status == 200 || status == 204;
      return {...responseJson, status: status, isSuccess: isSuccess};
    })
    .catch((error) => {
      console.log(error);
      throw error;
    });
}

export function getAccessToken() {
  //Access token is long lived
  let state = store.getState() as RootState;

  if (state.access_token) {
    let accessToken = state.access_token;
    console.log('accessToken', accessToken);
    return accessToken;
  } else {
    return '';
  }
}
