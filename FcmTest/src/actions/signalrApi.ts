import * as baseApi from './baseApi';
import config from './config';
import {busyUpdated} from './actionCreator';
import {Alert, ToastAndroid} from 'react-native';
import {
  Patient,
  HttpResponse,
  JoinWaitingRoomResponse,
  GenericMeasurement,
} from '../types';
import BridgeModule from '../nativemodules/BridgeModule';
import {BloodPressureRecord} from '@src/types/bluetooth';
import {FcmBridge} from '@src/nativemodules/FcmBridge';

const baseUrl = config.signalr;

function getAsync(route: string) {
  return baseApi.getAsync(route, baseUrl);
}

function putAsync(route: string, data: any) {
  return baseApi.putAsync(route, data, baseUrl);
}

function postAsync(route: string, data: any) {
  return baseApi.postAsync(route, data, baseUrl);
}

function deleteAsync(route: string, data: any) {
  return baseApi.deleteAsync(route, data, baseUrl);
}

/**
 * Notify signalr listeners of new measurement
 */
export function notifyMeasurement(payload: GenericMeasurement) {
  return postAsync('notify/measurement', payload)
    .then((result) => {
      console.log('Notify measurement success', result);

      return result;
    })
    .catch((error) => {
      console.log('Error', error);

      return error;
    });
}

/**
 * Notify blood pressure measurement
 */
export function notifyBloodPressure(payload: BloodPressureRecord) {
  return postAsync('notify/bloodpressure', payload).then((result) => {
    console.log('Notify Blood Pressure success', result);
    return result;
  });
}

/**
 * Join the Waiting Room
 * @param patientName Set to null if using medicareCardNumberRaw
 * @param doctorName The name of the requested doctor or null
 * @param medicareCardNumberRaw Set to null if using patientName
 */
export function joinWaitingRoom(
  patientName: string,
  doctorName: string,
  medicareCardNumberRaw: string,
) {
  return function (dispatch: any) {
    dispatch(busyUpdated(true));

    return FcmBridge.getRegistrationToken()
      .then((token) => {
        console.log('Registration token', token);
        return postAsync('waiting', {
          patientName: patientName,
          requestedDoctor: doctorName,
          medicareNumberRaw: medicareCardNumberRaw,
          registrationToken: token,
        });
      })
      .then((result: JoinWaitingRoomResponse) => {
        let ticketNumber = result.action.patient.ticketNumber;
        ToastAndroid.show(
          'Joined waiting room. Ticket #' + ticketNumber,
          ToastAndroid.SHORT,
        );
        BridgeModule.saveTicketNumber(ticketNumber);
        dispatch({type: 'TICKET_NUMBER_UPDATED', ticketNumber: ticketNumber});
        return result;
      })
      .catch((error) => {
        console.log('Error', error);

        return error;
      })
      .finally(() => {
        dispatch(busyUpdated(false));
      });
  };
}

export function leaveWaitingRoom(ticketNumber: number) {
  console.log('Leaving ticket number:', ticketNumber);
  return function (dispatch: any) {
    return deleteAsync('waiting', {ticketNumber: ticketNumber}).then(
      (result) => {
        console.log('Leave result', result);
        let ticketNumber = -1;
        BridgeModule.saveTicketNumber(ticketNumber);
        dispatch({type: 'TICKET_NUMBER_UPDATED', ticketNumber: ticketNumber});
        return result;
      },
    );
  };
}

export function getQueuePosition(ticketNumber: number) {
  return getAsync('ticketnumber/' + ticketNumber + '/queueposition');
}

interface IgnoreCallResponse extends HttpResponse {
  queuePosition?: number;
  patient?: Patient;
}
export function ignoreCall(ticketNumber: number): Promise<IgnoreCallResponse> {
  return postAsync('ticketnumber/' + ticketNumber + '/ignorecall', {}).then(
    (result: IgnoreCallResponse) => {
      console.log('Ignore call response', result);
      return result;
    },
  );
}
