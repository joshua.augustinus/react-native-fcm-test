import Config from 'react-native-config';

const platforms = {
  localhost: {
    signalr: 'http://localhost:9156',
    fcm: 'https://localhost:44361',
    user: Config.USER_URL,
  },
  azure: {
    signalr: Config.SIGNALR_URL,
    fcm: Config.FCM_URL,
    user: Config.USER_URL,
  },
};

console.log('Platforms', platforms.azure);

export default platforms.azure;
