import {DefaultTheme} from 'react-native-paper';

const theme = {
  ...DefaultTheme,

  colors: {
    ...DefaultTheme.colors,
    primary: '#02613f',
  },
};

export {theme};
