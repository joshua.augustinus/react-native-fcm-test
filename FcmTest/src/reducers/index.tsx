import {AnyAction} from 'redux';
import {RootState} from '../types';

// Be careful
// The default values will be loaded from loadAllSharedPreferences too
const initialState: RootState = {
  measurement: null,
  options: {playSounds: true, isCareProvider: false},
  isLoaded: false,
  username: null,
  access_token: null,
  isBusy: false,
  idClicked: -1,
  fcmProps: undefined,
  ticketNumber: 0,
  displayName: null,
  appHeaderVisibility: true,
};

const defaultReducer = (state = initialState, action: AnyAction): RootState => {
  let {type, ...rest} = action;
  switch (action.type) {
    case 'RESET_STORE':
      let newState = {
        ...initialState,
        options: {...initialState.options},
        isLoaded: true,
      };
      console.log('RESET_STORE', newState);
      return newState;
    case 'SAVED_STATE_LOADED':
      return {...state, ...action.data, isLoaded: true};
    case 'MEASUREMENT_UPDATED':
      console.log('inside reducer', action);
      return {...state, measurement: action.data};
    case 'BUSY_UPDATED':
      return {...state, isBusy: action.isBusy};
    case 'USERNAME_UPDATED':
      return {...state, username: action.data};
    case 'ACCESSTOKEN_UPDATED':
      return {...state, access_token: action.data};
    case 'OPTIONS_UPDATED':
      console.log('OPTIONS_UPDATED', action);
      return {...state, options: {...state.options, ...action.data}};
    case 'BUTTON_CLICKED':
      return {...state, ...rest};
    case 'TICKET_NUMBER_UPDATED':
      return {...state, ...rest};
    case 'DISPLAY_NAME_UPDATED':
      return {...state, ...rest};
    case 'APPHEADER_VISIBILITY_UPDATED':
      return {...state, appHeaderVisibility: action.appHeaderVisibility};
    default:
      return state;
  }
};

export default defaultReducer;
